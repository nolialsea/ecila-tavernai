const express = require('express');
const app = express();
const fs = require('fs');
const readline = require('readline');
const open = require('open');
const {Readable} = require("stream");

const rimraf = require("rimraf");
const multer = require("multer");
const extract = require('png-chunks-extract');
const encode = require('png-chunks-encode');
const PNGtext = require('png-chunk-text');
const StreamConcat = require("stream-concat");

const sharp = require('sharp');
sharp.cache(false);
const path = require('path');

const cookieParser = require('cookie-parser');
const crypto = require('crypto');
const ipaddr = require('ipaddr.js');

const config = require(path.join(process.cwd(), './config.conf'));
const server_port = config.port;
const whitelist = config.whitelist;
const whitelistMode = config.whitelistMode;
const autorun = config.autorun;


const Client = require('node-rest-client').Client;
const client = new Client();

const api_ecila = "http://localhost:3000"

let response_dw_bg;

let is_colab = false;
let charactersPath = 'public/characters/';
let chatsPath = 'public/chats/';
if (is_colab && process.env.googledrive === 2) {
    charactersPath = '/content/drive/MyDrive/TavernAI/characters/';
    chatsPath = '/content/drive/MyDrive/TavernAI/chats/';
}
const jsonParser = express.json({limit: '100mb'});
const urlencodedParser = express.urlencoded({extended: true, limit: '100mb'});

// CSRF Protection //
const doubleCsrf = require('csrf-csrf').doubleCsrf;

const CSRF_SECRET = crypto.randomBytes(8).toString('hex');
const COOKIES_SECRET = crypto.randomBytes(8).toString('hex');

const {invalidCsrfTokenError, generateToken, doubleCsrfProtection} = doubleCsrf({
    getSecret: () => CSRF_SECRET,
    cookieName: "X-CSRF-Token",
    cookieOptions: {
        httpOnly: true,
        sameSite: "strict",
        secure: false
    },
    size: 64,
    getTokenFromRequest: (req) => req.headers["x-csrf-token"]
});

app.get("/csrf-token", (req, res) => {
    res.json({
        "token": generateToken(res)
    });
});

app.use(cookieParser(COOKIES_SECRET));
app.use(doubleCsrfProtection);

// CORS Settings //
const cors = require('cors');
const axios = require("axios");
const CORS = cors({
    origin: 'null',
    methods: ['OPTIONS']
});

app.use(CORS);

app.use(function (req, res, next) { //Security
    let clientIp = req.connection.remoteAddress;
    let ip = ipaddr.parse(clientIp);
    // Check if the IP address is IPv4-mapped IPv6 address
    if (ip.kind() === 'ipv6' && ip.isIPv4MappedAddress()) {
        const ipv4 = ip.toIPv4Address().toString();
        clientIp = ipv4;
    } else {
        clientIp = ip;
        clientIp = clientIp.toString();
    }

    //clientIp = req.connection.remoteAddress.split(':').pop();
    if (whitelistMode === true && !whitelist.includes(clientIp)) {
        console.log('Forbidden: Connection attempt from ' + clientIp + '. If you are attempting to connect, please add your IP address in whitelist or disable whitelist mode in config.conf in root of TavernAI folder.\n');
        return res.status(403).send('<b>Forbidden</b>: Connection attempt from <b>' + clientIp + '</b>. If you are attempting to connect, please add your IP address in whitelist or disable whitelist mode in config.conf in root of TavernAI folder.');
    }
    next();
});

app.use((req, res, next) => {
    if (req.url.startsWith('/characters/') && is_colab && process.env.googledrive === 2) {

        const filePath = path.join(charactersPath, decodeURIComponent(req.url.substr('/characters'.length)));
        fs.access(filePath, fs.constants.R_OK, (err) => {
            if (!err) {
                res.sendFile(filePath);
            } else {
                res.send('Character not found: ' + filePath);
                //next();
            }
        });
    } else {
        next();
    }
});
app.use(express.static(__dirname + "/public", {refresh: true}));


app.use('/backgrounds', (req, res) => {
    const filePath = decodeURIComponent(path.join(process.cwd(), 'public/backgrounds', req.url.replace(/%20/g, ' ')));
    fs.readFile(filePath, (err, data) => {
        if (err) {
            res.status(404).send('File not found');
            return;
        }
        res.send(data);
    });
});

app.use('/characters', (req, res) => {
    const filePath = decodeURIComponent(path.join(process.cwd(), charactersPath, req.url.replace(/%20/g, ' ')));
    fs.readFile(filePath, (err, data) => {
        if (err) {
            res.status(404).send('File not found');
            return;
        }
        res.send(data);
    });
});
app.use(multer({dest: "uploads"}).single("avatar"));
app.get("/", function (request, response) {
    response.sendFile(__dirname + "/public/index.html");
});
app.get("/notes/*", function (request, response) {
    response.sendFile(__dirname + "/public" + request.url + ".html");
});

//***************** Main functions


async function charaWrite(img_url, data, target_img, response = undefined, mes = 'ok') {
    try {
        // Load the image in any format
        sharp.cache(false);
        const image = await sharp(img_url).resize(400, 600).toFormat('png').toBuffer();// old 170 234

        // Get the chunks
        const chunks = extract(image);
        const tEXtChunks = chunks.filter(chunk => chunk.create_date === 'tEXt');

        // Remove all existing tEXt chunks
        for (let tEXtChunk of tEXtChunks) {
            chunks.splice(chunks.indexOf(tEXtChunk), 1);
        }
        // Add new chunks before the IEND chunk
        const base64EncodedData = Buffer.from(data, 'utf8').toString('base64');
        chunks.splice(-1, 0, PNGtext.encode('chara', base64EncodedData));

        // fs.writeFileSync(charactersPath + target_img + '.png', new Buffer.from(encode(chunks)));
        if (response !== undefined) response.send(mes);
    } catch (err) {
        console.log(err);
        if (response !== undefined) response.send(err);
    }
}

function charaRead(img_url) {
    sharp.cache(false);
    const buffer = fs.readFileSync(img_url);
    const chunks = extract(buffer);

    const textChunks = chunks.filter(function (chunk) {
        return chunk.name === 'tEXt';
    }).map(function (chunk) {
        return PNGtext.decode(chunk.data);
    });

    return Buffer.from(textChunks[0].text, 'base64').toString('utf8');
}

app.post("/getcharacters", jsonParser, function (request, response) {
    fs.readdir(charactersPath, (err, files) => {
        if (err) {
            console.error(err);
            return;
        }

        const pngFiles = files.filter(file => file.endsWith('.png'));
        const characters = {};
        let i = 0;
        pngFiles.forEach(item => {
            const img_data = charaRead(charactersPath + item);
            try {
                let jsonObject = JSON.parse(img_data);
                jsonObject.avatar = item;
                characters[i] = jsonObject;
                i++;
            } catch (error) {
                if (error instanceof SyntaxError) {
                    console.log("String [" + (i) + "] is not valid JSON!");
                } else {
                    console.log("An unexpected error occurred: ", error);
                }
            }
        });
        response.send(JSON.stringify(characters));
    });
});
app.post("/getbackgrounds", jsonParser, function (request, response) {
    let images = getImages("public/backgrounds");
    if (is_colab === true) {
        images = ['tavern.png'];
    }
    response.send(JSON.stringify(images));

});
app.post("/iscolab", jsonParser, function (request, response) {
    let send_data = false;
    if (process.env.colaburl !== undefined) {
        send_data = String(process.env.colaburl).trim();
    }
    response.send({colaburl: send_data});

});
app.post("/getuseravatars", jsonParser, function (request, response) {
    const images = getImages("public/User Avatars");
    response.send(JSON.stringify(images));

});
app.post("/setbackground", jsonParser, function (request, response) {
    const bg = "#bg1 {background-image: linear-gradient(rgba(19,21,44,0.75), rgba(19,21,44,0.75)), url(../backgrounds/" + request.body.bg + ");}";
    fs.writeFile('public/css/bg_load.css', bg, 'utf8', function (err) {
        if (err) {
            response.send(err);
            return console.log(err);
        } else {
            response.send({result: 'ok'});
        }
    });

});
app.post("/delbackground", jsonParser, function (request, response) {
    if (!request.body) return response.sendStatus(400);
    rimraf('public/backgrounds/' + request.body.bg, (err) => {
        if (err) {
            response.send(err);
            return console.log(err);
        } else {
            response.send('ok');
        }
    });

});
app.post("/downloadbackground", urlencodedParser, function (request, response) {
    response_dw_bg = response;
    if (!request.body) return response.sendStatus(400);

    let filedata = request.file;
    let fileType = ".png";
    let img_file = "ai";
    let img_path = "public/img/";

    img_path = "uploads/";
    img_file = filedata.filename;
    if (filedata.mimetype === "image/jpeg") fileType = ".jpeg";
    if (filedata.mimetype === "image/png") fileType = ".png";
    if (filedata.mimetype === "image/gif") fileType = ".gif";
    if (filedata.mimetype === "image/bmp") fileType = ".bmp";
    fs.copyFile(img_path + img_file, 'public/backgrounds/' + img_file + fileType, (err) => {
        if (err) {

            return console.log(err);
        } else {
            response_dw_bg.send(img_file + fileType);
        }
    });


});

app.post("/savesettings", jsonParser, function (request, response) {


    fs.writeFile('public/settings.json', JSON.stringify(request.body), 'utf8', function (err) {
        if (err) {
            response.send(err);
            return console.log(err);
        } else {
            response.send({result: "ok"});
        }
    });

});
app.post('/getsettings', jsonParser, (request, response) => { //Wintermute's code
    const koboldai_settings = [];
    const koboldai_setting_names = [];
    const novelai_settings = [];
    const novelai_setting_names = [];
    const settings = fs.readFileSync('public/settings.json', 'utf8', (err, data) => {
        if (err) return response.sendStatus(500);

        return data;
    });
    //Kobold
    const files = fs
        .readdirSync('public/KoboldAI Settings')
        .sort(
            (a, b) =>
                new Date(fs.statSync(`public/KoboldAI Settings/${b}`).mtime) -
                new Date(fs.statSync(`public/KoboldAI Settings/${a}`).mtime)
        );

    files.forEach(item => {
        const file = fs.readFileSync(
            `public/KoboldAI Settings/${item}`,
            'utf8',
            (err, data) => {
                if (err) return response.sendStatus(500)

                return data;
            }
        );
        koboldai_settings.push(file);
        koboldai_setting_names.push(item.replace(/\.[^/.]+$/, ''));
    });

    //Novel
    const files2 = fs
        .readdirSync('public/NovelAI Settings')
        .sort(
            (a, b) =>
                new Date(fs.statSync(`public/NovelAI Settings/${b}`).mtime) -
                new Date(fs.statSync(`public/NovelAI Settings/${a}`).mtime)
        );

    files2.forEach(item => {
        const file2 = fs.readFileSync(
            `public/NovelAI Settings/${item}`,
            'utf8',
            (err, data) => {
                if (err) return response.sendStatus(500);

                return data;
            }
        );

        novelai_settings.push(file2);
        novelai_setting_names.push(item.replace(/\.[^/.]+$/, ''));
    });

    response.send({
        settings,
        koboldai_settings,
        koboldai_setting_names,
        novelai_settings,
        novelai_setting_names
    });
});


function getImages(path) {
    return fs.readdirSync(path).sort(function (a, b) {
        return new Date(fs.statSync(path + '/' + a).mtime) - new Date(fs.statSync(path + '/' + b).mtime);
    }).reverse();
}

app.get('/tts', jsonParser, async function (req, res) {
    try {
        const voiceId = req.query.voice_id
        const apiKey = req.query.access_token
        const stability = req.query.stability || "0"
        const similarityBoost = req.query.similarity || "0"
        const text = req.query.text

        const response = await axios.post(
            `https://api.elevenlabs.io/v1/text-to-speech/${voiceId}`,
            {
                text,
                "voice_settings": {
                    stability: parseInt(stability) / 100,
                    "similarity_boost": parseInt(similarityBoost) / 100
                }
            },
            {
                responseType: 'stream',
                headers: {
                    "accept": "audio/mpeg",
                    "Content-Type": "application/json",
                    "xi-api-key": apiKey
                }
            }).catch(err => console.error(err))

        const readable = Readable.from(response.data);

        readable.on('data', (chunk) => {
            res.write(chunk)
        })
        readable.on('error', () => {
            res.sendStatus(500)
        })
        readable.on('end', () => {
            res.end()
        })
    } catch (e) {
        res.sendStatus(500)
    }
})

app.get('/tts2', jsonParser, async function (req, res) {
    try {
        const apiKey = req.query.access_token
        const voiceId = req.query.voice_id
        const stability = req.query.stability || "0"
        const similarityBoost = req.query.similarity || "0"
        const thirdPersonVoice = req.query.third_person_voice_id
        const thirdPersonStability = req.query.third_person_stability || "0"
        const thirdPersonSimilarityBoost = req.query.third_person_similarity || "0"
        const synchronous = req.query?.synchronous === 'true'

        const text = req.query?.text?.trim()
        const firstPersonBits = text
                ?.split(/\*[^*]*\*/ig)  // keeps only text outside asterisks
                ?.map(s => s.trim())
                ?.filter(s => !!s)
            || []
        const thirdPersonBits = text
                ?.match(/\*([^*]*)\*/ig) // keeps only text inside asterisks
                ?.map(s => s.substring(1, s.length - 1)) // removed encasing asterisks
                ?.map(s => s.trim())
                ?.filter(s => !!s)
            || []

        let firstPersonTalking = !text.startsWith('*')


        try {
            const conversation = (firstPersonTalking ? [firstPersonBits, thirdPersonBits] : [thirdPersonBits, firstPersonBits])
                .reduce((r, a) => (a.forEach((a, i) => (r[i] = r[i] || []).push(a)), r), [])
                .reduce((a, b) => a.concat(b))

            firstPersonTalking = !firstPersonTalking // reverses order for alternation logic below (comma expression)

            res.header('Content-Type', 'audio/mp3')

            const readableStreams = conversation
                .map(async text => { // converts to array of readable streams
                        return Readable.from(
                            (await axios.post(
                                `https://api.elevenlabs.io/v1/text-to-speech/${(firstPersonTalking = !firstPersonTalking, firstPersonTalking) ? voiceId : thirdPersonVoice}`,
                                {
                                    text,
                                    "voice_settings": {
                                        stability: parseInt(firstPersonTalking ? stability : thirdPersonStability) / 100,
                                        "similarity_boost": parseInt(firstPersonTalking ? similarityBoost : thirdPersonSimilarityBoost) / 100
                                    }
                                },
                                {
                                    responseType: 'stream',
                                    headers: {
                                        "accept": "audio/mpeg",
                                        "Content-Type": "application/json",
                                        "xi-api-key": apiKey
                                    }
                                }).catch(err => console.error(err))).data
                        )
                    }
                )

            let streamIndex = 0
            const nextStream = () => {
                if (streamIndex === readableStreams.length) {
                    return null;
                }
                return readableStreams[streamIndex++]
            }

            const mergedStream = new StreamConcat(nextStream);

            let data = []

            mergedStream.on('data', (chunk) => {
                if (synchronous) {
                    data.push(chunk)
                } else {
                    res.write(chunk)
                }
            })
            mergedStream.on('error', (err) => {
                throw err
            })
            mergedStream.on('end', () => {
                if (synchronous) {
                    data.map(d => res.write(d))
                }
                res.end()
            })
        } catch (e) {
            throw e
        }
    } catch (e) {
        console.error(e)
        res.sendStatus(500)
    }
})

app.post('/textToImageNovelAI', jsonParser, async function (req, res) {
    try {
        const prompt = req.body?.prompt
        const width = req.body?.width ?? 512
        const height = req.body?.height ?? 512
        const response = await axios.post(
            api_ecila + "/api/v1/generate/textToImageNovelAI",
            {
                access_token: req.body?.access_token,
                prompt,
                width,
                height
            },
            {
                headers: {
                    "accept": "image/png",
                    "Content-Type": "application/json"
                }
            }).catch(err => console.error(err))
        res.send(response?.data)
    } catch (e) {
        res.sendStatus(500)
    }
})

app.post('/textToImageSD', jsonParser, async function (req, res) {
    try {
        const prompt = req.body?.prompt
        const width = req.body?.width ?? 512
        const height = req.body?.height ?? 512
        const response = await axios.post(
            api_ecila + "/api/v1/generate/textToImageSD",
            {
                access_token: req.body?.access_token,
                prompt,
                width,
                height
            },
            {
                headers: {
                    "accept": "image/png",
                    "Content-Type": "application/json"
                }
            }).catch(err => console.error(err))
        res.send(response?.data)
    } catch (e) {
        res.sendStatus(500)
    }
})

app.post("/getstatus", jsonParser, function (request, response_getstatus = response) {
    if (!request.body) return response_getstatus.sendStatus(400);
    const args = {
        data: {
            access_token: request.body.access_token
        },
        headers: {"Content-Type": "application/json"}
    };

    client.post(api_ecila + "/api/v1/user/connect", args, function (data, response) {
        if (response.statusCode == 200) {
            data.result = "Connected!"
        } else {
            data.result = "no_connection";
        }
        response_getstatus.send(data);
    }).on('error', function (err) {
        response_getstatus.send({result: "no_connection"});
    });
});

app.post("/get_heat", jsonParser, function (request, response_getstatus = response) {
    if (!request.body) return response_getstatus.sendStatus(400);
    const args = {
        data: {
            access_token: request.body.access_token
        },
        headers: {"Content-Type": "application/json"}
    };

    client.post(api_ecila + "/api/v1/user/getHeat", args, function (data, response) {
        response_getstatus.status(response.statusCode).send(data)
    }).on('error', function (err) {
        response_getstatus.send({result: "no_connection"});
    });
});

app.post("/generate_ecila", jsonParser, function (request, responseEcila = response) {
    if (!request.body) return responseEcila.sendStatus(400);

    const data = request.body
    const args = {
        data,
        headers: {"Content-Type": "application/json"}
    };
    client.post(api_ecila + "/api/v1/generate/chatbotMessage", args, function (data, response) {
        if (response.statusCode === 200) {
            responseEcila.send(data);
        }
        if (response.statusCode === 400) {
            console.log('Validation error');
            responseEcila.status(response.statusCode).send({error: true})
        }
        if (response.statusCode === 401) {
            console.log('Access Token is incorrect');
            responseEcila.status(response.statusCode).send({error: true})
        }
        if (response.statusCode === 402) {
            console.log('An active subscription is required to access this endpoint');
            responseEcila.status(response.statusCode).send({error: true})
        }
        if (response.statusCode === 403) {
            console.log('Too hot!');
            responseEcila.status(response.statusCode).send({error: true})
        }
        if (response.statusCode === 500 || response.statusCode === 409) {
            responseEcila.status(response.statusCode).send({error: true})
        }
    }).on('error', function (err) {
        responseEcila.send({error: true});
    });
});


app.post("/getallchatsofchatacter", jsonParser, function (request, response) {
    if (!request.body) return response.sendStatus(400);

    const char_dir = (request.body.avatar_url).replace('.png', '');
    fs.readdir(chatsPath + char_dir, (err, files) => {
        if (err) {
            console.error(err);
            response.send({error: true});
            return;
        }

        // filter for JSON files
        const jsonFiles = files.filter(file => path.extname(file) === '.jsonl');

        // print the sorted file names
        const chatData = {};
        let ii = jsonFiles.length;
        for (let i = jsonFiles.length - 1; i >= 0; i--) {
            const file = jsonFiles[i];

            const fileStream = fs.createReadStream(chatsPath + char_dir + '/' + file);
            const rl = readline.createInterface({
                input: fileStream,
                crlfDelay: Infinity
            });

            let lastLine;

            rl.on('line', (line) => {
                lastLine = line;
            });

            rl.on('close', () => {
                if (lastLine) {
                    let jsonData = JSON.parse(lastLine);
                    if (jsonData.name !== undefined) {
                        chatData[i] = {};
                        chatData[i]['file_name'] = file;
                        chatData[i]['mes'] = jsonData['mes'];
                        ii--;
                        if (ii === 0) {
                            response.send(chatData);
                        }
                    } else {
                        return;
                    }
                }
                rl.close();
            });
        }
    });

});

function getPngName(file) {
    let i = 1;
    let base_name = file;
    while (fs.existsSync(charactersPath + file + '.png')) {
        file = base_name + i;
        i++;
    }
    return file;
}

app.post("/importcharacter", urlencodedParser, function (request, response) {
    if (!request.body) return response.sendStatus(400);

    let png_name = '';
    let filedata = request.file;
    const format = request.body.file_type;
    if (filedata) {
        if (format === 'json') {
            fs.readFile('./uploads/' + filedata.filename, 'utf8', (err, data) => {
                if (err) {
                    console.log(err);
                    response.send({error: true});
                }
                let jsonData
                try {
                    jsonData = JSON.parse(data);
                } catch {
                }


                if (jsonData?.name !== undefined) {
                    png_name = getPngName(jsonData.name);
                    let char = {
                        "name": jsonData.name,
                        "description": jsonData.description ?? '',
                        "personality": jsonData.personality ?? '',
                        "first_mes": jsonData.first_mes ?? '',
                        "avatar": 'none',
                        "chat": Date.now(),
                        "mes_example": jsonData.mes_example ?? '',
                        "scenario": jsonData.scenario ?? '',
                        "create_date": Date.now()
                    };
                    char = JSON.stringify(char);
                    charaWrite('./public/img/fluffy.png', char, png_name, response, {file_name: png_name});
                } else if (jsonData?.char_name !== undefined) {//json Pygmalion notepad
                    png_name = getPngName(jsonData.char_name);
                    let char = {
                        "name": jsonData.char_name,
                        "description": jsonData.char_persona ?? '',
                        "personality": '',
                        "first_mes": jsonData.char_greeting ?? '',
                        "avatar": 'none',
                        "chat": Date.now(),
                        "mes_example": jsonData.example_dialogue ?? '',
                        "scenario": jsonData.world_scenario ?? '',
                        "create_date": Date.now()
                    };
                    char = JSON.stringify(char);
                    charaWrite('./public/img/fluffy.png', char, png_name, response, {file_name: png_name});
                } else {
                    console.log('Incorrect character format .json');
                    response.send({error: true});
                }
            });
        } else {
            try {

                const img_data = charaRead('./uploads/' + filedata.filename);
                let jsonData = JSON.parse(img_data);
                png_name = getPngName(jsonData.name);

                if (jsonData.name !== undefined) {
                    let char = {
                        "name": jsonData.name,
                        "description": jsonData.description ?? '',
                        "personality": jsonData.personality ?? '',
                        "first_mes": jsonData.first_mes ?? '',
                        "avatar": 'none',
                        "chat": Date.now(),
                        "mes_example": jsonData.mes_example ?? '',
                        "scenario": jsonData.scenario ?? '',
                        "create_date": Date.now()
                    };
                    char = JSON.stringify(char);
                    charaWrite('./uploads/' + filedata.filename, char, png_name, response, {file_name: png_name});
                }
            } catch (err) {
                console.log(err);
                response.send({error: true});
            }
        }
    }
});

app.post("/importchat", urlencodedParser, function (request, response) {
    if (!request.body) return response.sendStatus(400);

    const format = request.body.file_type;
    let filedata = request.file;
    let avatar_url = (request.body.avatar_url).replace('.png', '');
    let ch_name = request.body.character_name;
    if (filedata) {

        if (format === 'json') {
            fs.readFile('./uploads/' + filedata.filename, 'utf8', (err, data) => {

                if (err) {
                    console.log(err);
                    response.send({error: true});
                }

                const jsonData = JSON.parse(data);
                const new_chat = [];
                if (jsonData.histories !== undefined) {
                    let i = 0;
                    new_chat[i] = {};
                    new_chat[0]['user_name'] = 'You';
                    new_chat[0]['character_name'] = ch_name;
                    new_chat[0]['create_date'] = Date.now();
                    i++;
                    jsonData.histories.histories[0].msgs.forEach(function (item) {
                        new_chat[i] = {};
                        if (item.src?.is_human) {
                            new_chat[i]['name'] = 'You';
                        } else {
                            new_chat[i]['name'] = ch_name;
                        }
                        new_chat[i]['is_user'] = item.src.is_human;
                        new_chat[i]['is_name'] = true;
                        new_chat[i]['send_date'] = Date.now();
                        new_chat[i]['mes'] = item.text;
                        i++;
                    });
                    const chatJsonlData = new_chat.map(JSON.stringify).join('\n');
                    fs.writeFile(chatsPath + avatar_url + '/' + Date.now() + '.jsonl', chatJsonlData, 'utf8', function (err) {
                        if (err) {
                            response.send(err);
                            return console.log(err);
                        } else {
                            response.send({res: true});
                        }
                    });

                } else {
                    response.send({error: true});
                    return;
                }

            });
        }
        if (format === 'jsonl') {
            const fileStream = fs.createReadStream('./uploads/' + filedata.filename);
            const rl = readline.createInterface({
                input: fileStream,
                crlfDelay: Infinity
            });

            rl.once('line', (line) => {
                let jsonData = JSON.parse(line);

                if (jsonData.user_name !== undefined) {
                    fs.copyFile('./uploads/' + filedata.filename, chatsPath + avatar_url + '/' + Date.now() + '.jsonl', (err) => {
                        if (err) {
                            response.send({error: true});
                            return console.log(err);
                        } else {
                            response.send({res: true});
                            return;
                        }
                    });
                } else {
                    return;
                }
                rl.close();
            });
        }

    }

});


app.listen(server_port, function () {
    if (process.env.colab !== undefined) {
        if (process.env.colab === 2) {
            is_colab = true;
        }
    }
    console.log('Launching...');
    if (autorun) open('http:127.0.0.1:' + server_port);
    console.log('TavernAI started: http://127.0.0.1:' + server_port);
    if (fs.existsSync('public/characters/update.txt') && !is_colab) {
        convertStage1();
    }

});

//#####################CONVERTING IN NEW FORMAT########################

let charactersB = {};//B - Backup
let character_ib = 0;

const directoriesB = {};


function convertStage1() {
    const directories = getDirectories2("public/characters");
    charactersB = {};
    character_ib = 0;
    getCharaterFile2(directories, 0);
}

function convertStage2() {
    let mes = true;
    for (const key in directoriesB) {
        if (mes) {
            console.log('***');
            console.log('The update of the character format has begun...');
            console.log('***');
            mes = false;
        }

        const char = JSON.parse(charactersB[key]);
        char.create_date = Date.now();
        charactersB[key] = JSON.stringify(char);
        let avatar = 'public/img/fluffy.png';
        if (char.avatar !== 'none') {
            avatar = 'public/characters/' + char.name + '/avatars/' + char.avatar;
        }

        charaWrite(avatar, charactersB[key], directoriesB[key]);

        const files = fs.readdirSync('public/characters/' + directoriesB[key] + '/chats');
        if (!fs.existsSync(chatsPath + char.name)) {
            fs.mkdirSync(chatsPath + char.name);
        }
        files.forEach(function (file) {
            // Read the contents of the file
            const fileContents = fs.readFileSync('public/characters/' + directoriesB[key] + '/chats/' + file, 'utf8');

            // Iterate through the array of strings and parse each line as JSON
            let chat_data = JSON.parse(fileContents);
            let new_chat_data = [];
            let this_chat_user_name = 'You';
            let is_pass_0 = false;
            if (chat_data[0].indexOf('<username-holder>') !== -1) {
                this_chat_user_name = chat_data[0].substr('<username-holder>'.length, chat_data[0].length);
                is_pass_0 = true;
            }
            let i = 0;
            let ii = 0;
            new_chat_data[i] = {user_name: 'You', character_name: char.name, create_date: Date.now()};
            i++;
            ii++;
            chat_data.forEach(function (mes) {
                if (!(i === 1 && is_pass_0)) {
                    if (mes.indexOf('<username-holder>') === -1 && mes.indexOf('<username-idkey>') === -1) {
                        new_chat_data[ii] = {};
                        let is_name = false;
                        if (mes.trim().indexOf(this_chat_user_name + ':') !== 0) {
                            if (mes.trim().indexOf(char.name + ':') === 0) {
                                mes = mes.replace(char.name + ':', '');
                                is_name = true;
                            }
                            new_chat_data[ii]['name'] = char.name;
                            new_chat_data[ii]['is_user'] = false;
                            new_chat_data[ii]['is_name'] = is_name;
                            new_chat_data[ii]['send_date'] = Date.now();

                        } else {
                            mes = mes.replace(this_chat_user_name + ':', '');
                            new_chat_data[ii]['name'] = 'You';
                            new_chat_data[ii]['is_user'] = true;
                            new_chat_data[ii]['is_name'] = true;
                            new_chat_data[ii]['send_date'] = Date.now();

                        }
                        new_chat_data[ii]['mes'] = mes.trim();
                        ii++;
                    }
                }
                i++;

            });
            const jsonlData = new_chat_data.map(JSON.stringify).join('\n');
            // Write the contents to the destination folder
            fs.writeFileSync(chatsPath + char.name + '/' + file + 'l', jsonlData);
        });
        console.log(char.name + ' update!');
    }
    fs.unlinkSync('public/characters/update.txt');
    if (mes === false) {
        console.log('***');
        console.log('Сharacter format update completed successfully!');
        console.log('***');
        console.log('Now you can delete these folders, they are no longer used by TavernAI:');
    }
    for (const key in directoriesB) {
        console.log('public/characters/' + directoriesB[key]);
    }
}

function getDirectories2(path) {
    return fs.readdirSync(path)
        .filter(function (file) {
            return fs.statSync(path + '/' + file).isDirectory();
        })
        .sort(function (a, b) {
            return new Date(fs.statSync(path + '/' + a).mtime) - new Date(fs.statSync(path + '/' + b).mtime);
        })
        .reverse();
}

function getCharaterFile2(directories, i) {
    if (directories.length > i) {
        fs.stat('public/characters/' + directories[i] + '/' + directories[i] + ".json", function (err, stat) {
            if (err == null) {
                fs.readFile('public/characters/' + directories[i] + '/' + directories[i] + ".json", 'utf8', (err, data) => {
                    if (err) {
                        console.error(err);
                        return;
                    }
                    //console.log(data);
                    if (!fs.existsSync('public/characters/' + directories[i] + '.png')) {
                        charactersB[character_ib] = {};
                        charactersB[character_ib] = data;
                        directoriesB[character_ib] = directories[i];
                        character_ib++;
                    }
                    i++;
                    getCharaterFile2(directories, i);
                });
            } else {
                i++;
                getCharaterFile2(directories, i);
            }
        });
    } else {
        convertStage2();
    }
}

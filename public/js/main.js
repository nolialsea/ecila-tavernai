let tutorial_finished = localStorage.getItem('tutorial_finished') || false
let bg_menu_toggle = false;
let saveLocally = true
const default_user_name = localStorage.getItem('your_name') || "You";
let name1 = default_user_name;
let name2 = "Ecila";
let chat = [{
    name: 'Ecila',
    is_user: false,
    is_name: true,
    create_date: 0,
    mes: 'Hey, remember me?\nWell, this is my new home! My own Tavern! Finally, I can get the <b>fuck</b> out of discord! *she giggles*' +
        '<img id="img_front_page" src="img/star_dust_city.png" width=80% style="width: 512px; height: 512 px; opacity:1; display:block;border-radius:5px;margin-top:25px;margin-bottom:23px; margin-left: 45px;margin-right: auto;" alt="star_dust_city">'
        + '\n<a id="verson" style="color:rgb(229, 224, 216,0.8);" href="https://www.patreon.com/evilnoli" target="_blank"><img src="img/patreon.webp" style="width: 25px;height: auto;display:inline-block; opacity:0.7;" alt="patreon">&nbsp;Check out Evil Noli\'s Patreon!</a>'
        + '<div id="characloud_url" style="margin-right:10px;margin-top:0;float:right; height:25px;cursor: pointer;opacity: 0.99;display:inline-block;">'
        + '<div style="vertical-align: top;display:inline-block;"><a style="color:rgb(229, 224, 216,0.8);" target="_blank" href="https://ko-fi.com/evilnoli">Or pay him a coffee!&nbsp;<img src="img/ko-fi-logo.png" style="width: 25px;height: auto;display:inline-block; opacity:0.7;" alt="ko-fi"></a></div></div><br><br><br><br>'
}];

const interactiveTutorial = [
    {
        message: {
            name: 'Ecila',
            is_user: false,
            is_name: true,
            create_date: 0,
            continue: true,
            mes: `It looks like it's your first time here! Please, let me show you the ropes...`
        }
    }, {
        message: {
            name: 'Ecila',
            is_user: false,
            is_name: true,
            create_date: 0,
            continue: false,
            mes: `Before anything, you'll need to enter your API key to access all the AI features! Here, I'll just pull the input for you!`
                + `<br/><h4 style="display: inline-block">API key:</h4><input id="api_key_ecila_alt" name="api_key_ecila" class="text_pole" maxlength="500" size="55" value="" autocomplete="off"><input id="api_button_ecila_alt" type="submit" value="Connect">`
        }
    }, {
        message: {
            name: 'Ecila',
            is_user: false,
            is_name: true,
            create_date: 0,
            continue: true,
            mes: `*she makes ridiculous beeping noises with her mouth* Ding! Yup, that's a valid key! Now I need to ask you a few questions about yourself if you don't mind, this step is necessary to generate an accurate representation of you for the AI!`
        }
    }, {
        message: {
            name: 'Ecila',
            is_user: false,
            is_name: true,
            create_date: 0,
            continue: false,
            mes: `Alright, first question! What is your gender?`
        }
    }, {
        onUserInput: (input) => {

        },
        message: {
            name: 'Ecila',
            is_user: false,
            is_name: true,
            create_date: 0,
            mes: `Alright, first question! What is your gender?`
        }
    }
]

let chat_create_date = 0;
const default_ch_mes = "Hello";
let count_view_mes = 0;
let generatedPromtCache = '';
let characters = [];
characters[null] = {description: "[ Character: Ecila; gender: female; species: AI tentacle-girl; note: Ecila is a friendly Artificial Intelligence designed to guide users through the chatbot application. ]"}
let this_chid = null;
const default_avatar = 'img/fluffy.png';
let is_mes_reload_avatar = false;

let is_advanced_char_open = false;

let menu_type = '';//what is selected in the menu
let selected_button = '';//which button pressed
//create pole save
let create_save_name = '';
let create_save_description = '';
let create_save_personality = '';
let create_save_first_message = '';
let create_save_voice = '';
let create_save_voice_third_person = '';
let create_save_avatar = '';
let create_save_scenario = '';
let create_save_mes_example = '';

let timerSaveEdit;
const durationSaveEdit = 200;
//animation right menu
const animation_rm_duration = 200;
const animation_rm_easing = "";

let popup_type = "";
let bg_file_for_del = '';
let online_status = 'no_connection';

let api_server = "";
let api_ecila = "http://localhost:3000"
let is_get_status = false;
let is_get_status_novel = false;
let is_get_status_ecila = false;
let is_api_button_press = false;
let is_api_button_press_ecila = false;

let is_send_press = false;//Send generation
let add_mes_without_animation = false;

let this_del_mes = 0;

let this_edit_mes_chname = '';
let this_delete_mes_chname = '';
let this_edit_mes_id;
let this_delete_mes_id;
let this_continue_mes_id = undefined;
let this_retry_mes_id = undefined;

//settings
let settings;
let koboldai_settings;
let koboldai_setting_names;
let preset_settings = 'gui';
let user_avatar = 'you1.png';
let temp = 0.5;
let amount_gen = 80;
let max_context = 2048;//2048;
let rep_pen = 1;
let rep_pen_size = 100;

let if_typing_text = false;

let anchor_order = 0;
let style_anchor = true;
let character_anchor = true;
let token;

function uuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}

$(document).ready(function () {
    const converter = new showdown.Converter();

    let main_api = 'ecila';
    //novel settings
    let temp_novel = 0.5;
    let rep_pen_novel = 1;
    let rep_pen_size_novel = 100;

    let api_key_novel = "";
    let api_key_ecila = localStorage.getItem("api_key_ecila");
    $('#api_key_ecila').val(api_key_ecila)

    let your_voice = localStorage.getItem("your_voice") || 'Pega'
    if (your_voice) {
        $('#settings_your_voice').val(your_voice)
    }
    $('#settings_your_voice').on('change', v => {
        your_voice = v.target.value
        localStorage.setItem("your_voice", your_voice)
    })

    let model_novel = "euterpe-v2";
    let preset_settings_novel = 'Classic-Krake';

    localStorage.setItem('current_character_name', '')

    let bg = localStorage.getItem('background')
    if (bg) {
        $('#bg1').css('background-image', 'linear-gradient(rgba(19,21,44,0.75), rgba(19,21,44,0.75)), url("backgrounds/' + bg + '")');
        $('#bg2').css('background-image', 'linear-gradient(rgba(19,21,44,0.75), rgba(19,21,44,0.75)), url("backgrounds/' + bg + '")');
    }

    //css
    let bg1_toggle = true;
    const css_mes_bg = $('<div class="mes"></div>').css('background');
    const css_send_form_display = $('<div id=send_form></div>').css('display');

    $('#result_count').val(localStorage.getItem('result_count') || 1)
    $('#settings_enable_alternative_tts').val(localStorage.getItem('settings_enable_alternative_tts') || 'false')
    $('#settings_11lab_api_key').val(localStorage.getItem('settings_11lab_api_key') || '')
    $('#settings_11lab_voice_id').val(localStorage.getItem('settings_11lab_voice_id') || '')
    $('#settings_11lab_your_voice_id').val(localStorage.getItem('settings_11lab_your_voice_id') || '')
    $('#settings_11lab_stability').val(localStorage.getItem('settings_11lab_stability') || '0')
    $('#settings_11lab_similarity').val(localStorage.getItem('settings_11lab_similarity') || '0')
    $('#settings_11lab_your_stability').val(localStorage.getItem('settings_11lab_your_stability') || '0')
    $('#settings_11lab_your_similarity').val(localStorage.getItem('settings_11lab_your_similarity') || '0')
    $('#settings_enable_autoplay_tts').val(localStorage.getItem('settings_enable_autoplay_tts') || 'false')
    $('#settings_enable_tts_streamer_mode').val(localStorage.getItem('settings_enable_tts_streamer_mode') || 'false')


    $.ajaxPrefilter((options, originalOptions, xhr) => {
        xhr.setRequestHeader("X-CSRF-Token", token);
    });

    $.get("/csrf-token")
        .then(data => {
            token = data.token;
            getSettings("def");
            getCharacters();

            printMessages();
            getBackgrounds();
            getUserAvatars();
        });


    $('#avatar_url_confirm').click(function () {
        let characters_ = localStorage.getItem('characters') || []
        if (typeof characters_ === "string") {
            characters_ = JSON.parse(characters_)
        }
        const url = $('#avatar_url').val()
        for (let i in characters_) {
            if (characters_[i].name === characters[this_chid]?.name) {
                characters[this_chid].avatar = url
                characters[this_chid].avatar_url = url
                characters_[i].avatar = url
                characters_[i].avatar_url = url
            }
        }

        localStorage.setItem(`characters`, JSON.stringify(characters_))
        printCharaters()
    });

    function checkOnlineStatus() {
        if (online_status === 'no_connection') {
            $("#online_status_indicator").css("background-color", "red");
            $("#online_status").css("opacity", 0.3);
            $("#online_status_text").html("No connection...");
            $("#online_status_indicator2").css("background-color", "red");
            $("#online_status_text2").html("No connection...");
            $("#online_status_indicator3").css("background-color", "red");
            $("#online_status_text3").html("No connection...");
            $("#online_status_indicator3_ecila").css("background-color", "red");
            $("#online_status_text3_ecila").html("No connection...");
            is_get_status = false;
            is_get_status_novel = false;
            is_get_status_ecila = false;
        } else {
            $("#online_status_indicator").css("background-color", "black");
            $("#online_status").css("opacity", 0.0);
            $("#online_status_text").html("");
            $("#online_status_indicator2").css("background-color", "green");
            $("#online_status_text2").html(online_status);
            $("#online_status_indicator3").css("background-color", "green");
            $("#online_status_text3").html(online_status);
            $("#online_status_indicator3_ecila").css("background-color", "green");
            $("#online_status_text3_ecila").html(online_status);
        }
    }

    function charaFormatData(data) {
        return {
            "name": data.ch_name,
            "description": data.description,
            "personality": data.personality,
            "first_mes": data.first_mes,
            "avatar": data.avatar_url || data.avatar,
            "chat": Date.now(),
            "mes_example": data.mes_example,
            "scenario": data.scenario,
            "create_date": Date.now(),
            "character_voice": data.character_voice,
            "character_voice_third_person": data.character_voice_third_person
        };
    }

    async function getStatus() {
        if (is_get_status) {
            function success(data) {
                console.log(data)
                online_status = data?.result;
                if (online_status === undefined) {
                    online_status = 'no_connection';
                }
                resultCheckStatus();
                if (online_status !== 'no_connection') {
                    setTimeout(getStatus, 3000);
                }
            }

            if (true) { // TODO: fix and clean up
                online_status = 'Connected!';
                resultCheckStatus();
                return
            }

            if (main_api === 'ecila' && $('#api_key_ecila').val()) {
                jQuery.ajax({
                    type: 'POST',
                    url: '/getstatus',
                    data: JSON.stringify({
                        access_token: api_key_ecila,
                        "X-CSRF-Token": token
                    }),
                    beforeSend: function () {
                        if (is_api_button_press) {
                        }
                    },
                    cache: false,
                    dataType: "json",
                    crossDomain: true,
                    contentType: "application/json",
                    success,
                    error: function (jqXHR, exception) {
                        console.log(exception);
                        console.log(jqXHR);
                        online_status = 'no_connection';

                        resultCheckStatus();
                    }
                });

                success({result: "Connected!"})
                return
            }

        } else {
            if (is_get_status_ecila !== true) {
                online_status = 'no_connection';
            }
        }
    }

    function resultCheckStatus() {
        is_api_button_press = false;
        checkOnlineStatus();
        $("#api_loading").css("display", 'none');
        $("#api_button").css("display", 'inline-block');
    }

    function printCharaters() {
        $("#rm_print_charaters_block").empty();
        characters.forEach(function (item, i, arr) {
            if (!item || !item.create_date) return
            let this_avatar = default_avatar;
            if (item?.avatar !== 'none') {
                const avatar_url = item?.avatar_url?.startsWith?.('http') ? item?.avatar_url : null
                const avatar = item?.avatar?.startsWith?.('http') ? item?.avatar : null
                this_avatar = avatar_url ?? avatar ?? ("characters/" + item?.avatar + "#" + Date.now());
            }
            if (i !== null) {
                $("#rm_print_charaters_block").prepend('<div class=character_select chid=' + i + '><div class=avatar><img src="' + this_avatar + '"></div><div class=ch_name>' + item?.name + '</div></div>');
            }
        });
    }

    async function getCharacters() {
        let getData
        const charactersString = localStorage.getItem('characters')
        if (saveLocally && charactersString) {
            getData = JSON.parse(charactersString) || []
        } else {
            const response = await fetch("/getcharacters", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "X-CSRF-Token": token
                },
                body: "{}"
            });
            if (response.ok === true) {
                getData = await response.json();
            }

            if (getData) {
                localStorage.setItem('characters', JSON.stringify(Object.values(getData)))
            }
        }

        if (!getData || getData.length === 0) return

        const load_ch_count = Object.getOwnPropertyNames(getData);

        for (let i = 0; i < load_ch_count.length; i++) {
            characters[i] = getData[i];
        }

        let previous_chid = this_chid
        let previous_character = null
        if (this_chid !== null && this_chid !== undefined) {
            previous_character = characters[previous_chid]
        }

        characters.sort((a, b) => a.create_date - b.create_date);

        const newCharacterIndex = characters.indexOf(previous_character)
        if (newCharacterIndex !== -1) {
            this_chid = newCharacterIndex
        }

        if (this_chid !== undefined) $("#avatar_url_pole").val(characters[this_chid].avatar);
        printCharaters();
    }

    async function getBackgrounds() {
        const response = await fetch("/getbackgrounds", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "X-CSRF-Token": token
            },
            body: "{}"
        });
        if (response.ok === true) {
            const getData = await response.json();
            for (let i = 0; i < getData.length; i++) {
                $("#bg_menu_content").append("<div class=bg_example><img bgfile='" + getData[i] + "' class=bg_example_img src='backgrounds/" + getData[i] + "'></div>");
            }
        }
    }

    async function setBackground(bg) {
        localStorage.setItem('background', bg)
    }

    async function delBackground(bg) {
        return
    }


    async function printMessages() {
        add_mes_without_animation = true
        for (let item of chat) {
            addOneMessage(item);
        }
        add_mes_without_animation = false
    }

    function clearChat() {
        count_view_mes = 0;
        $('#chat').html('');
    }

    function messageFormating(mes, ch_name) {
        if (this_chid !== null) mes = mes.replaceAll("<", "&lt;").replaceAll(">", "&gt;");//for Chloe
        if (this_chid === null) {
            mes = mes.replace(/\*\*(.+?)\*\*/g, '<b>$1</b>').replace(/\*(.+?)\*/g, '<i>$1</i>').replace(/\n/g, '<br/>');

        } else {
            mes = converter.makeHtml(mes);
            mes = mes.replace(/\n/g, '<br/>');
        }


        if (ch_name !== name1) {
            mes = mes.replaceAll(name2 + ":", "");
        }
        return mes;
    }

    function addOneMessage(mes) {
        if (!mes) return
        let messageText = mes['mes'];
        let characterName = name1;
        generatedPromtCache = '';
        let avatarImg = "User Avatars/" + user_avatar;
        const messageUuid = uuidv4()

        if (!mes['is_user']) {
            if (this_chid === null) {
                avatarImg = "img/ecila.png";
            } else {
                if (characters[this_chid].avatar !== 'none') {
                    avatarImg = characters[this_chid].avatar?.startsWith?.('http') ?
                        characters[this_chid].avatar
                        : ("characters/" + characters[this_chid].avatar);
                    if (is_mes_reload_avatar !== false) {
                        avatarImg += "#" + is_mes_reload_avatar;
                    }
                } else {
                    avatarImg = "img/fluffy.png";
                }
            }
            characterName = name2;

            if (characterName === 'Ecila' && !add_mes_without_animation) {

                const nsfw = window.location.href.includes('?nsfw=true')
                jQuery.ajax({
                    type: 'POST',
                    url: '/textToImageNovelAI',
                    data: JSON.stringify({
                        access_token: api_key_ecila,
                        "X-CSRF-Token": token,
                        "prompt": `closeup portrait, Ecila, female AI, happy, dancing, laughing, tavern in the background, thin frame with small breasts, tall, [[[tentacles]]] redhead red tentacle hair, green eyes, light brown skin tone, ${!nsfw ? 'black dress' : 'nude, naked'}, cute smile, cute face, {{{realism}}}, {{dramatic lighting}}, by greg rutkowski`,
                        "width": 512,
                        "height": 768,
                        "negativePrompt": nsfw ? "big breasts" : undefined
                    }),
                    beforeSend: function () {

                    },
                    cache: false,
                    dataType: "json",
                    crossDomain: true,
                    contentType: "application/json",
                    success: function (result) {
                        console.log("result", result)
                        // $('#img_front_page').attr("src", 'data:image/png;base64,' + result.data)
                    },
                    error: function (jqXHR, exception) {
                        // console.log(exception);
                        // console.log(jqXHR);
                        if (jqXHR.status === 200) {
                            avatarImg = 'data:image/png;base64,' + jqXHR.responseText
                            const selector = $(`#${messageUuid}`)
                            selector.transition({
                                opacity: 0.0,
                                duration: 250,
                                easing: "",
                                complete: async function () {
                                    selector.attr("src", avatarImg)
                                    await new Promise(r => setTimeout(r, 500))
                                    selector.transition({
                                        opacity: 1.0,
                                        duration: 250,
                                        easing: "",
                                        complete: function () {
                                        }
                                    });
                                }
                            });
                        } else {
                            online_status = 'no_connection';
                            resultCheckStatus();
                        }
                    }
                })
            }
        }

        if (true) {
            //Formating
            if (count_view_mes === 0) {
                messageText = messageText.replace(/{{user}}/gi, name1);
                messageText = messageText.replace(/{{char}}/gi, name2);
                messageText = messageText.replace(/<USER>/gi, name1);
                messageText = messageText.replace(/<BOT>/gi, name2);
            }
            messageText = messageFormating(messageText, characterName);

            const divAvatar = `<div class=avatar><img id="${messageUuid}" src='${avatarImg}'></div>`

            $("#chat").append(
                "<div class='mes' mesid="
                + count_view_mes
                + " ch_name="
                + characterName
                + ">"
                + "<div class='character_name' hidden>" + characterName + "</div>"
                + "<div class='for_checkbox'></div>"
                + "<input type='checkbox' class='del_checkbox'>"
                + divAvatar
                + "<div class=mes_block>"
                + "<div class=ch_name>"
                + characterName
                + "<div title=Delete class=mes_delete>"
                + "<img src=img/cancel.png style='width:20px;height:20px;'>"
                + "</div>"
                + "<div class=mes_delete_cancel>"
                + "<img src=img/cancel.png>"
                + "</div>"
                + "<div class=mes_delete_done>"
                + "<img src=img/done.png>"
                + "</div>"

                + "<div title=Edit class=mes_edit>"
                + "<img src=img/scroll.png style='width:20px;height:20px;'>"
                + "</div>"
                + "<div class=mes_edit_cancel>"
                + "<img src=img/cancel.png>"
                + "</div>"
                + "<div class=mes_edit_done>"
                + "<img src=img/done.png>"
                + "</div>"

                + "<div title=Continue class=mes_continue>"
                + "<img src=img/tri.png style='width:20px;height:20px;'>"
                + "</div>"
                + "<div class=mes_continue_cancel>"
                + "<img src=img/cancel.png>"
                + "</div>"
                + "<div class=mes_continue_done>"
                + "<img src=img/done.png>"
                + "</div>"

                + "<div title=Retry class=mes_retry>"
                + "<img src=img/regenerate.png style='width:20px;height:20px;'>"
                + "</div>"
                + "<div class=mes_retry_cancel>"
                + "<img src=img/cancel.png>"
                + "</div>"
                + "<div class=mes_retry_done>"
                + "<img src=img/done.png>"
                + "</div>"

                + "<div class='copy_message'><img src='img/clipboard.png'></div>"
                + "<div class='speak_message'><img src='img/speaker-emoji.png'></div>"

                + "<div class=loading_message>"
                + "<img src=img/load4.gif>"
                + "</div>"

                + `<audio controls><source type="audio/mpeg-2">Your browser does not support the audio element.</audio>`

                + "</div>"
                + "<div class=mes_text>"
                + "</div>"
                + "</div>"
                + "</div>"
            );


            if (!if_typing_text) {
                $("#chat").children().filter('[mesid="' + count_view_mes + '"]').children('.mes_block').children('.mes_text').append(messageText);
            } else {
                typeWriter($("#chat").children().filter('[mesid="' + count_view_mes + '"]').children('.mes_block').children('.mes_text'), messageText, 50, 0);
            }

            count_view_mes++;
            if (!add_mes_without_animation) {
                const selector = $('#chat').children().last()
                selector.css("opacity", 0.0);
                selector.transition({
                    opacity: 1.0,
                    duration: 700,
                    easing: "",
                    complete: function () {
                    }
                });
            } else {
                // add_mes_without_animation = false;
            }
            const $textchat = $('#chat');
            $textchat.scrollTop($textchat[0].scrollHeight);


            const autoPlayTTS = $('#settings_enable_autoplay_tts').val() === 'true'
                && (!mes['is_user'] || $('#settings_enable_tts_streamer_mode').val() === 'true')
                && !mes['mes'].startsWith('[')
            if (!add_mes_without_animation && autoPlayTTS) {
                $("#chat").children().last().children('.mes_block').children('.ch_name').children('.speak_message').click()
            }
        }
    }

    function typeWriter(target, text, speed, i) {
        if (i < text.length) {
            target.html(target.html() + text.charAt(i));
            i++;
            setTimeout(() => typeWriter(target, text, speed, i), speed);
        }
    }

    $("#send_but").click(function () {
        if (is_send_press === false) {
            is_send_press = true;
            Generate();
        }
    });

    async function Generate(type) {
        let textareaText;

        const condition = online_status !== 'no_connection'// && this_chid !== undefined
        if (!condition) {
            if (false && this_chid === null) {
                //send ch sel
                popup_type = 'char_not_selected';
                callPopup('<h3>Сharacter is not selected</h3>');
            }
            is_send_press = false;
            return
        }

        const selector = $("#send_textarea")
        textareaText = selector.val();
        selector.val('');

        $("#send_but").css("display", "none");
        $("#loading_mes").css("display", "block");


        //*********************************
        //PRE FORMATING STRING
        //*********************************
        if (textareaText !== "") {
            chat[chat.length] = {};
            chat[chat.length - 1]['name'] = name1;
            chat[chat.length - 1]['is_user'] = true;
            chat[chat.length - 1]['is_name'] = true;
            chat[chat.length - 1]['send_date'] = Date.now();
            chat[chat.length - 1]['mes'] = textareaText;
            addOneMessage(chat[chat.length - 1]);
        }
        let charDescription = $.trim(characters[this_chid].description);

        if (charDescription !== undefined) {
            if (charDescription.length > 0) {
                charDescription = charDescription.replace(/{{user}}/gi, name1);
                charDescription = charDescription.replace(/{{char}}/gi, name2);
                charDescription = charDescription.replace(/<USER>/gi, name1);
                charDescription = charDescription.replace(/<BOT>/gi, name2);
            }
        }

        runGenerate();

        function runGenerate() {
            const generate_data = {
                access_token: api_key_ecila,
                bot: {
                    name: name2,
                    context: charDescription
                },
                history: chat
                    .map(m => {
                        return {author: m.name === "You" || m.name === "User" ? name1 : m.name, content: m.mes}
                    })
                    .filter(m => !!m)
                    .map(m => {
                        return {
                            author: m?.author,
                            content: m?.content
                                .replace(/{{user}}/gi, name1)
                                .replace(/{{char}}/gi, name2)
                                .replace(/<USER>/gi, name1)
                                .replace(/<BOT>/gi, name2)
                        }
                    })
                    .slice(-100)
            }
            const generate_url = '/generate_ecila';

            jQuery.ajax({
                type: 'POST',
                url: generate_url,
                data: JSON.stringify(generate_data),
                beforeSend: function () {
                },
                cache: false,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    let getMessage;
                    is_send_press = false;
                    if (data.error !== true) {
                        if (main_api === 'ecila') {
                            if (data) {
                                console.log(`+${data.heat} heat`)
                                gauge.setValue(gauge.getValue() + parseFloat(data.currentHeat))
                                getMessage = data.messages[0]?.content
                            }
                        }

                        //Formating
                        getMessage = $.trim(getMessage);

                        let this_mes_is_name = true;
                        if (getMessage.indexOf(name2 + ":") === 0) {
                            getMessage = getMessage.replace(name2 + ':', '');
                            getMessage = getMessage.trimStart();
                        } else {
                            this_mes_is_name = false;
                        }

                        if (getMessage.length > 0) {
                            chat[chat.length] = {};
                            chat[chat.length - 1]['name'] = name2;
                            chat[chat.length - 1]['is_user'] = false;
                            chat[chat.length - 1]['is_name'] = this_mes_is_name;
                            chat[chat.length - 1]['send_date'] = Date.now();
                            getMessage = $.trim(getMessage);
                            chat[chat.length - 1]['mes'] = getMessage;
                            addOneMessage(chat[chat.length - 1]);
                            $("#send_but").css("display", "block");
                            $("#loading_mes").css("display", "none");
                            saveChat();
                        }
                    } else {
                        $("#send_but").css("display", "block");
                        $("#loading_mes").css("display", "none");
                    }
                },
                error: function (jqXHR, exception) {
                    $("#send_textarea").removeAttr('disabled');
                    is_send_press = false;
                    $("#send_but").css("display", "block");
                    $("#loading_mes").css("display", "none");
                    console.log(exception);
                    console.log(jqXHR);
                }
            });
        }
    }

    async function generateMessage(charName, charDescription, chat, continueLastMessage = false, result_count = 1) {
        const generate_data = {
            access_token: api_key_ecila,
            continue_last_message: continueLastMessage,
            result_count,
            bot: {
                name: charName,
                context: charDescription
            },
            history: chat
                .map(m => {
                    return {
                        author: m.author ?? (m.name === "You" || m.name === "User" ? name1 : m.name),
                        content: (m.content ?? m.mes)
                            ?.replace?.(/{{user}}/gi, name1)
                            .replace(/{{char}}/gi, name2)
                            .replace(/<USER>/gi, name1)
                            .replace(/<BOT>/gi, name2)
                    }
                })
                .slice(-100)
        }
        const generate_url = '/generate_ecila';

        const settings = {
            method: 'POST',
            body: JSON.stringify(generate_data),
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                "X-CSRF-Token": token
            }
        }

        try {
            const fetchResponse = await fetch(generate_url, settings);
            const data = await fetchResponse.json();
            if (data) {
                console.log(`+${data.heat} heat`)
                gauge.setValue(gauge.getValue() + parseFloat(data.heat))
                return data.messages;
            }
        } catch (e) {
            return e;
        }
    }

    async function saveChat() {
        chat = chat.filter(i => !!i)
        chat.forEach(function (item, i) {
            if (item['is_user']) {
                chat[i]['mes'] = item['mes'].replace(name1 + ':', default_user_name + ':');
                chat[i]['name'] = default_user_name;
            }
        });
        const save_chat = [{
            user_name: default_user_name,
            character_name: name2,
            create_date: chat_create_date
        }, ...chat];

        if (saveLocally) {
            const characterName = characters[this_chid].name
            localStorage.setItem(`chat_${characterName}`, JSON.stringify(save_chat))
        } else {
            jQuery.ajax({
                type: 'POST',
                url: '/savechat',
                data: JSON.stringify({
                    ch_name: characters[this_chid].name,
                    file_name: characters[this_chid].chat,
                    chat: save_chat,
                    avatar_url: characters[this_chid].avatar
                }),
                beforeSend: function () {
                },
                cache: false,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {

                },
                error: function (jqXHR, exception) {

                    console.log(exception);
                    console.log(jqXHR);
                }
            });
        }
    }

    async function getChat() {
        if (saveLocally) {
            const characterName = characters[this_chid]?.name
            const dataString = localStorage.getItem(`chat_${characterName}`)
            const data = dataString ? JSON.parse(dataString) : null
            if (data && data[0] !== undefined) {
                for (let key in data) {
                    chat.push(data[key]);
                }
                chat_create_date = chat[0]['create_date'];
                chat.shift();
            } else {
                chat_create_date = Date.now();
            }
            getChatResult();
        } else {
            jQuery.ajax({
                type: 'POST',
                url: '/getchat',
                data: JSON.stringify({
                    ch_name: characters[this_chid].name,
                    file_name: characters[this_chid].chat,
                    avatar_url: characters[this_chid].avatar
                }),
                beforeSend: function () {
                },
                cache: false,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    if (data[0] !== undefined) {
                        for (let key in data) {
                            chat.push(data[key]);
                        }
                        chat_create_date = chat[0]['create_date'];
                        chat.shift();
                    } else {
                        chat_create_date = Date.now();
                    }
                    getChatResult();
                    saveChat();
                },
                error: function (jqXHR, exception) {
                    getChatResult();
                    console.log(exception);
                    console.log(jqXHR);
                }
            });
        }
    }

    function getChatResult() {
        name2 = characters[this_chid].name;
        if (chat.length > 1) {

            chat.forEach(function (item, i) {
                if (item?.['is_user']) {
                    chat[i]['mes'] = item['mes'].replace(default_user_name + ':', name1 + ':');
                    chat[i]['name'] = name1;
                }
            });
        } else {
            chat[0] = {};
            chat[0]['name'] = name2;
            chat[0]['is_user'] = false;
            chat[0]['is_name'] = true;
            chat[0]['send_date'] = Date.now();
            if (characters[this_chid].first_mes !== "") {
                chat[0]['mes'] = characters[this_chid].first_mes;
            } else {
                chat[0]['mes'] = default_ch_mes;
            }
        }
        printMessages();
        select_selected_character(this_chid);
    }

    $("#send_textarea").keypress(function (e) {
        if (e.which === 13 && !e.shiftKey && is_send_press === false) {
            is_send_press = true;
            e.preventDefault();
            Generate();
        }
    });

    //menu buttons
    const seleced_button_style = {color: "#bcc1c8"};
    const deselected_button_style = {color: "#565d66"};
    $("#rm_button_create").children("h2").css(seleced_button_style);
    $("#rm_button_characters").children("h2").css(seleced_button_style);
    $("#rm_button_settings").click(function () {
        selected_button = 'settings';
        menu_type = 'settings';
        $("#rm_charaters_block").css("display", "none");
        $("#rm_api_block").css("display", "block");

        $('#rm_api_block').css('opacity', 0.0);
        $('#rm_api_block').transition({
            opacity: 1.0,
            duration: animation_rm_duration,
            easing: animation_rm_easing,
            complete: function () {
            }
        });

        $("#rm_ch_create_block").css("display", "none");
        $("#rm_info_block").css("display", "none");

        $("#rm_button_characters").children("h2").css(deselected_button_style);
        $("#rm_button_settings").children("h2").css(seleced_button_style);
        $("#rm_button_selected_ch").children("h2").css(deselected_button_style);
    });
    $("#rm_button_characters").click(function () {
        selected_button = 'characters';
        select_rm_characters();
    });
    $("#rm_button_back").click(function () {
        selected_button = 'characters';
        select_rm_characters();
    });
    $("#rm_button_create").click(function () {
        selected_button = 'create';
        select_rm_create();
    });
    $("#rm_button_selected_ch").click(function () {
        selected_button = 'character_edit';
        select_selected_character(this_chid);
    });

    function updateHeatGauge() {
        jQuery.ajax({
            type: 'POST',
            url: '/get_heat',
            data: JSON.stringify({
                access_token: api_key_ecila
            }),
            beforeSend: function () {
            },
            cache: false,
            dataType: "json",
            crossDomain: true,
            contentType: "application/json",
            success: function (data) {
                if (typeof data.heat === "number") {
                    gauge.setValue(data.heat)
                    console.log("Current heat:", data.heat)
                }
            },
            error: function (jqXHR, exception) {
                console.log(exception);
                console.log(jqXHR);
            }
        });
    }


    $("#gauge").click(updateHeatGauge)

    function select_rm_create() {
        menu_type = 'create';
        if (selected_button === 'create') {
            if (create_save_avatar !== '') {
                $("#add_avatar_button").get(0).files = create_save_avatar;
                read_avatar_load($("#add_avatar_button").get(0));
            }
        }
        $("#rm_charaters_block").css("display", "none");
        $("#rm_api_block").css("display", "none");
        $("#rm_ch_create_block").css("display", "block");

        $('#rm_ch_create_block').css('opacity', 0.0);
        $('#rm_ch_create_block').transition({
            opacity: 1.0,
            duration: animation_rm_duration,
            easing: animation_rm_easing,
            complete: function () {
            }
        });
        $("#rm_info_block").css("display", "none");

        $("#delete_button_div").css("display", "none");
        $("#create_button").css("display", "block");
        $("#create_button").attr("value", "Create");
        $('#result_info').html('&nbsp;');
        $("#rm_button_characters").children("h2").css(deselected_button_style);
        $("#rm_button_settings").children("h2").css(deselected_button_style);
        $("#rm_button_selected_ch").children("h2").css(deselected_button_style);

        //create text poles
        $("#rm_button_back").css("display", "inline-block");
        $("#character_import_button").css("display", "inline-block");
        $("#character_popup_text_h3").text('Create character');
        $("#character_name_pole").val(create_save_name);
        $("#description_textarea").val(create_save_description);
        $("#personality_textarea").val(create_save_personality);
        $("#firstmessage_textarea").val(create_save_first_message);
        $("#character_voice").val(create_save_voice);
        $("#character_voice_third_person").val(create_save_voice_third_person)
        $("#scenario_pole").val(create_save_scenario);
        if ($.trim(create_save_mes_example).length === 0) {
            $("#mes_example_textarea").val('<START>');
        } else {
            $("#mes_example_textarea").val(create_save_mes_example);
        }
        $("#avatar_div").css("display", "block");
        $("#avatar_load_preview").attr('src', default_avatar);
        $("#name_div").css("display", "block");

        $("#form_create").attr("actiontype", "createcharacter");
    }

    function select_rm_characters() {

        menu_type = 'characters';
        $("#rm_charaters_block").css("display", "block");
        $('#rm_charaters_block').css('opacity', 0.0);
        $('#rm_charaters_block').transition({
            opacity: 1.0,
            duration: animation_rm_duration,
            easing: animation_rm_easing,
            complete: function () {
            }
        });

        $("#rm_api_block").css("display", "none");
        $("#rm_ch_create_block").css("display", "none");
        $("#rm_info_block").css("display", "none");

        $("#rm_button_characters").children("h2").css(seleced_button_style);
        $("#rm_button_settings").children("h2").css(deselected_button_style);
        $("#rm_button_selected_ch").children("h2").css(deselected_button_style);
    }

    function select_rm_info(text) {
        $("#rm_charaters_block").css("display", "none");
        $("#rm_api_block").css("display", "none");
        $("#rm_ch_create_block").css("display", "none");
        $("#rm_info_block").css("display", "flex");

        $("#rm_info_text").html('<h3>' + text + '</h3>');

        $("#rm_button_characters").children("h2").css(deselected_button_style);
        $("#rm_button_settings").children("h2").css(deselected_button_style);
        $("#rm_button_selected_ch").children("h2").css(deselected_button_style);
    }

    function select_selected_character(chid) { //character select

        select_rm_create();
        menu_type = 'character_edit';
        $("#delete_button_div").css("display", "block");
        $("#rm_button_selected_ch").children("h2").css(seleced_button_style);

        let display_name = characters[chid]?.name;

        $("#rm_button_selected_ch").children("h2").text(display_name);

        //create text poles
        $("#rm_button_back").css("display", "none");
        $("#create_button").attr("value", "Save");
        $("#create_button").css("display", "none");
        let i = 0;
        while ($("#rm_button_selected_ch").width() > 170 && i < 100) {
            display_name = display_name.slice(0, display_name.length - 2);
            $("#rm_button_selected_ch").children("h2").text($.trim(display_name) + '...');
            i++;
        }
        $("#add_avatar_button").val('');

        $('#character_popup_text_h3').text(characters[chid].name);
        $("#character_name_pole").val(characters[chid].name);
        $("#description_textarea").val(characters[chid].description);
        $("#avatar_url").val(characters[chid].avatar);
        $("#personality_textarea").val(characters[chid].personality);
        $("#firstmessage_textarea").val(characters[chid].first_mes);
        $("#character_voice").val(characters[chid].character_voice || 'alice');
        $("#character_voice_third_person").val(characters[chid].character_voice_third_person || 'Aini');
        $("#scenario_pole").val(characters[chid].scenario);
        $("#mes_example_textarea").val(characters[chid].mes_example);
        $("#selected_chat_pole").val(characters[chid].chat);
        $("#create_date_pole").val(characters[chid].create_date);
        $("#avatar_url_pole").val(characters[chid].avatar);
        $("#chat_import_avatar_url").val(characters[chid].avatar);
        $("#chat_import_character_name").val(characters[chid].name);
        let this_avatar = default_avatar;
        if (characters[chid].avatar !== 'none') {
            this_avatar = "characters/" + characters[chid].avatar;
        }
        $("#avatar_load_preview").attr('src', this_avatar + "#" + Date.now());
        $("#name_div").css("display", "none");

        $("#form_create").attr("actiontype", "editcharacter");
    }

    $(document).on('click', '.character_select', function () {
        if (this_chid !== $(this).attr("chid")) {
            if (!is_send_press) {
                this_edit_mes_id = undefined;
                selected_button = 'character_edit';
                this_chid = $(this).attr("chid");
                clearChat();
                chat.length = 0;
                getChat();
                if (characters[this_chid]) {
                    localStorage.setItem('current_character_name', characters[this_chid].name)
                }
            }
        } else {
            selected_button = 'character_edit';
            select_selected_character(this_chid);
        }
    });
    let scroll_holder = 0;
    let is_use_scroll_holder = false;
    $(document).on('input', '.edit_textarea', function () {
        scroll_holder = $("#chat").scrollTop();
        $(this).height(0).height(this.scrollHeight);
        is_use_scroll_holder = true;
    });
    $("#chat").on("scroll", function () {
        if (is_use_scroll_holder) {
            $("#chat").scrollTop(scroll_holder);
            is_use_scroll_holder = false;
        }

    });
    $(document).on('click', '.del_checkbox', function () {
        $('.del_checkbox').each(function () {
            $(this).prop("checked", false);
            $(this).parent().css('background', css_mes_bg);
        });
        $(this).parent().css('background', "#791b31");
        let i = $(this).parent().attr('mesid');
        this_del_mes = i;
        while (i < chat.length) {
            $(".mes[mesid='" + i + "']").css('background', "#791b31");
            $(".mes[mesid='" + i + "']").children('.del_checkbox').prop("checked", true);
            i++;
        }
    });
    $(document).on('click', '#user_avatar_block .avatar', function () {
        user_avatar = $(this).attr("imgfile");
        $('.mes').each(function () {
            if ($(this).attr('ch_name') === name1) {
                $(this).children('.avatar').children('img').attr('src', 'User Avatars/' + user_avatar);
            }
        });
        saveSettings();

    });
    $('#logo_block').click(function (event) {
        if (!bg_menu_toggle) {
            $('#bg_menu_button').transition({perspective: '100px', rotate3d: '1,1,0,180deg'});
            $('#bg_menu_content').transition({
                opacity: 1.0, height: '90vh',
                duration: 340,
                easing: 'in',
                complete: function () {
                    bg_menu_toggle = true;
                    $('#bg_menu_content').css("overflow-y", "auto");
                }
            });
        } else {
            $('#bg_menu_button').transition({perspective: '100px', rotate3d: '1,1,0,360deg'});
            $('#bg_menu_content').css("overflow-y", "hidden");
            $('#bg_menu_content').transition({
                opacity: 0.0, height: '0px',
                duration: 340,
                easing: 'in',
                complete: function () {
                    bg_menu_toggle = false;
                }
            });
        }
    });
    $(document).on('click', '.bg_example_img', function () {
        const this_bgfile = $(this).attr("bgfile");
        let number_bg
        let target_opacity
        if (bg1_toggle === true) {
            bg1_toggle = false;
            number_bg = 2;
            target_opacity = 1.0;
        } else {
            bg1_toggle = true;
            number_bg = 1;
            target_opacity = 0.0;
        }
        $('#bg2').stop();
        $('#bg2').transition({
            opacity: target_opacity,
            duration: 1300,//animation_rm_duration,
            easing: "linear",
            complete: function () {
                $("#options").css('display', 'none');
            }
        });


        $('#bg' + number_bg).css('background-image', 'linear-gradient(rgba(19,21,44,0.75), rgba(19,21,44,0.75)), url("backgrounds/' + this_bgfile + '")');
        setBackground(this_bgfile);
    });

    $(document).on('click', '.bg_example_cross', function () {
        bg_file_for_del = $(this);
        popup_type = 'del_bg';
        callPopup('<h3>Delete the background?</h3>');

    });

    $("#advanced_div").click(function () {
        if (!is_advanced_char_open) {
            is_advanced_char_open = true;
            $('#character_popup').css('display', 'grid');
            $('#character_popup').css('opacity', 0.0);
            $('#character_popup').transition({
                opacity: 1.0,
                duration: animation_rm_duration,
                easing: animation_rm_easing
            });
        } else {
            is_advanced_char_open = false;
            $('#character_popup').css('display', 'none');
        }
    });

    $("#character_cross").click(function () {
        is_advanced_char_open = false;
        $('#character_popup').css('display', 'none');
    });
    $("#character_popup_ok").click(function () {
        is_advanced_char_open = false;
        $('#character_popup').css('display', 'none');
    });

    $("#dialogue_popup_ok").click(async function () {
        $("#shadow_popup").css('display', 'none');
        $("#shadow_popup").css('opacity:', 0.0);
        if (popup_type === 'del_bg') {
            delBackground(bg_file_for_del.attr("bgfile"));
            bg_file_for_del.parent().remove();
        }
        if (popup_type === 'del_ch') {
            const msg = jQuery('#form_create').serialize(); // ID form
            if (saveLocally) {
                const formData = new FormData($("#form_create").get(0))

                const obj = {}
                for (const [key, value] of formData) {
                    obj[key] = value
                }
                const character = charaFormatData(obj)
                const charactersString = localStorage.getItem('characters')
                const characters = charactersString ? JSON.parse(charactersString) : [] || []

                const filteredCharacters = characters
                    .filter(c => c.name !== character.name)
                    .filter(c => !!c && c.name)

                localStorage.setItem('characters', JSON.stringify(filteredCharacters))
                await new Promise(r => setTimeout(r, 200))
                location.reload()
            } else {
                jQuery.ajax({
                    method: 'POST',
                    url: '/deletecharacter',
                    beforeSend: function () {
                        select_rm_info("Character deleted");
                    },
                    data: msg,
                    cache: false,
                    success: function (html) {
                        location.reload();
                    }
                });
            }
        }
        if (popup_type === 'new_chat' && this_chid !== undefined && menu_type !== "create") {//Fix it; New chat doesn't create while open create character menu
            clearChat();
            chat.length = 0;
            characters[this_chid].chat = Date.now();
            $("#selected_chat_pole").val(characters[this_chid].chat);
            timerSaveEdit = setTimeout(() => {
                $("#create_button").click();
            }, durationSaveEdit);
            getChat();

        }
    });
    $("#dialogue_popup_cancel").click(function () {
        $("#shadow_popup").css('display', 'none');
        $("#shadow_popup").css('opacity:', 0.0);
        popup_type = '';
    });

    function callPopup(text) {
        $("#dialogue_popup_cancel").css("display", "inline-block");
        switch (popup_type) {

            case 'char_not_selected':
                $("#dialogue_popup_ok").css("background-color", "#191b31CC");
                $("#dialogue_popup_ok").text("Ok");
                $("#dialogue_popup_cancel").css("display", "none");
                break;

            case 'new_chat':

                $("#dialogue_popup_ok").css("background-color", "#191b31CC");
                $("#dialogue_popup_ok").text("Yes");
                break;
            default:
                $("#dialogue_popup_ok").css("background-color", "#791b31");
                $("#dialogue_popup_ok").text("Delete");

        }
        $("#dialogue_popup_text").html(text);
        $("#shadow_popup").css('display', 'block');
        $('#shadow_popup').transition({
            opacity: 1.0,
            duration: animation_rm_duration,
            easing: animation_rm_easing
        });
    }

    function read_bg_load(input) {
        if (input.files && input.files[0]) {

            const reader = new FileReader();
            reader.onload = function (e) {

                $('#bg_load_preview')
                    .attr('src', e.target.result)
                    .width(103)
                    .height(83);
                const formData = new FormData($("#form_bg_download").get(0));
                jQuery.ajax({
                    type: 'POST',
                    url: '/downloadbackground',
                    data: formData,
                    beforeSend: function () {
                    },
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (html) {
                        setBackground(html);
                        if (bg1_toggle === true) {
                            bg1_toggle = false;
                            number_bg = 2;
                            target_opacity = 1.0;
                        } else {
                            bg1_toggle = true;
                            number_bg = 1;
                            target_opacity = 0.0;
                        }
                        $('#bg2').transition({
                            opacity: target_opacity,
                            duration: 1300,//animation_rm_duration,
                            easing: "linear",
                            complete: function () {
                                $("#options").css('display', 'none');
                            }
                        });
                        $('#bg' + number_bg).css('background-image', 'linear-gradient(rgba(19,21,44,0.75), rgba(19,21,44,0.75)), url(' + e.target.result + ')');
                        $("#form_bg_download").after("<div class=bg_example><img bgfile='" + html + "' class=bg_example_img src='backgrounds/" + html + "'></div>");
                    },
                    error: function (jqXHR, exception) {
                        console.log(exception);
                        console.log(jqXHR);
                    }
                });

            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#add_bg_button").change(function () {
        read_bg_load(this);
    });

    function read_avatar_load(input) {
        if (input.files && input.files[0]) {
            const reader = new FileReader();
            if (selected_button === 'create') {
                create_save_avatar = input.files;
            }
            reader.onload = function (e) {
                if (selected_button === 'character_edit') {
                    timerSaveEdit = setTimeout(() => {
                        $("#create_button").click();
                    }, durationSaveEdit);
                }
                $('#avatar_load_preview')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#add_avatar_button").change(function () {
        is_mes_reload_avatar = Date.now();
        read_avatar_load(this);
    });
    $("#form_create").submit(function (e) {
        $('#rm_info_avatar').html('');
        const formData = new FormData($("#form_create").get(0));
        let obj = {}
        for (const [key, value] of formData) {
            obj[key] = value
        }
        obj.avatar_url = $('#avatar_url').val()
        const character = charaFormatData(obj)
        const charactersString = localStorage.getItem('characters')
        const characters = charactersString ? JSON.parse(charactersString) : [] || []

        if ($("#form_create").attr("actiontype") === "createcharacter") {
            if ($("#character_name_pole").val().length > 0) {
                characters.push(character)

                $('#create_button').attr('disabled', true);
                $('#create_button').attr('value', 'Creating...')

                localStorage.setItem(`characters`, JSON.stringify(characters))

                $('#character_cross').click();
                $("#character_name_pole").val('');
                create_save_name = '';
                $("#description_textarea").val('');
                create_save_description = '';
                $("#personality_textarea").val('');
                create_save_personality = '';
                $("#firstmessage_textarea").val('');
                create_save_first_message = '';

                $("#character_voice").val('');
                create_save_voice = '';

                $("#character_voice_third_person").val('');
                create_save_voice_third_person = '';

                $("#character_popup_text_h3").text('Create character');

                $("#scenario_pole").val('');
                create_save_scenario = '';
                $("#mes_example_textarea").val('');
                create_save_mes_example = '';

                create_save_avatar = '';

                $('#create_button').removeAttr("disabled");
                $("#add_avatar_button").replaceWith($("#add_avatar_button").val('').clone(true));
                $('#create_button').attr('value', 'Create');
                $('#rm_info_block').transition({opacity: 0, duration: 0});
                const $prev_img = $('#avatar_div_div').clone();
                $('#rm_info_avatar').append($prev_img);
                select_rm_info("Character created");

                $('#rm_info_block').transition({opacity: 1.0, duration: 2000});
                getCharacters();
            } else {
                $('#result_info').html("Name not entered");
            }
        } else {
            if (saveLocally) {
                for (let i in characters) {
                    if (characters[i].name === character.name) {
                        characters[i] = character
                    }
                }
                localStorage.setItem(`characters`, JSON.stringify(characters))
                getCharacters();
            } else {
                jQuery.ajax({
                    type: 'POST',
                    url: '/editcharacter',
                    data: formData,
                    beforeSend: function () {
                        $('#create_button').attr('disabled', true);
                        $('#create_button').attr('value', 'Save');
                    },
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function () {

                        $('.mes').each(function () {
                            if ($(this).attr('ch_name') !== name1) {
                                $(this).children('.avatar').children('img').attr('src', $('#avatar_load_preview').attr('src'));
                            }
                        });
                        if (chat.length === 1) {

                            let this_ch_mes = default_ch_mes;
                            if ($('#firstmessage_textarea').val() !== "") {
                                this_ch_mes = $('#firstmessage_textarea').val();
                            }
                            if (this_ch_mes !== $.trim($("#chat").children('.mes').children('.mes_block').children('.mes_text').text())) {
                                clearChat();
                                chat.length = 0;
                                chat[0] = {};
                                chat[0]['name'] = name2;
                                chat[0]['is_user'] = false;
                                chat[0]['is_name'] = true;
                                chat[0]['mes'] = this_ch_mes;
                                add_mes_without_animation = true;
                                addOneMessage(chat[0]);
                            }
                        }
                        $('#create_button').removeAttr("disabled");

                    },
                    error: function (jqXHR, exception) {
                        $('#create_button').removeAttr("disabled");
                        $('#result_info').html("<font color=red>Error: no connection</font>");
                    }
                });

                getCharacters();

                $("#add_avatar_button").replaceWith($("#add_avatar_button").val('').clone(true));

                $('#create_button').attr('value', 'Save');
            }
        }
    });
    $("#delete_button").click(function () {
        popup_type = 'del_ch';
        callPopup('<h3>Delete the character?</h3>');
    });
    $("#rm_info_button").click(function () {
        $('#rm_info_avatar').html('');
        select_rm_characters();
    });
    //@@@@@@@@@@@@@@@@@@@@@@@@
    //character text poles creating and editing save
    $('#character_name_pole').on('change keyup paste', function () {
        if (menu_type === 'create') {
            create_save_name = $('#character_name_pole').val();
        }

    });
    $('#description_textarea').on('keyup paste cut', function () {//change keyup paste cut
        if (menu_type === 'create') {
            create_save_description = $('#description_textarea').val();
        } else {
            timerSaveEdit = setTimeout(() => {
                $("#create_button").click();
            }, durationSaveEdit);
        }
    });
    $('#personality_textarea').on('keyup paste cut', function () {
        if (menu_type === 'create') {

            create_save_personality = $('#personality_textarea').val();
        } else {
            timerSaveEdit = setTimeout(() => {
                $("#create_button").click();
            }, durationSaveEdit);
        }
    });
    $('#scenario_pole').on('keyup paste cut', function () {
        if (menu_type === 'create') {

            create_save_scenario = $('#scenario_pole').val();
        } else {
            timerSaveEdit = setTimeout(() => {
                $("#create_button").click();
            }, durationSaveEdit);
        }
    });
    $('#mes_example_textarea').on('keyup paste cut', function () {
        if (menu_type === 'create') {

            create_save_mes_example = $('#mes_example_textarea').val();
        } else {
            timerSaveEdit = setTimeout(() => {
                $("#create_button").click();
            }, durationSaveEdit);
        }
    });
    $('#firstmessage_textarea').on('keyup paste cut', function () {

        if (menu_type === 'create') {
            create_save_first_message = $('#firstmessage_textarea').val();
        } else {
            timerSaveEdit = setTimeout(() => {
                $("#create_button").click();
            }, durationSaveEdit);
        }
    });

    $('#character_voice').on('keyup paste cut', function () {
        if (menu_type === 'create') {
            create_save_voice = $('#character_voice').val();
        } else {
            timerSaveEdit = setTimeout(() => {
                $("#create_button").click();
            }, durationSaveEdit);
        }
    });

    $('#character_voice_third_person').on('keyup paste cut', function () {
        if (menu_type === 'create') {
            create_save_voice_third_person = $('#character_voice_third_person').val();
        } else {
            timerSaveEdit = setTimeout(() => {
                $("#create_button").click();
            }, durationSaveEdit);
        }
    });

    function connectEcila() {
        if ($('#api_url_text').val() !== '') {
            $("#api_loading").css("display", 'inline-block');
            api_server = $('#api_url_text').val();
            api_server = $.trim(api_server);
            if (api_server.substr(api_server.length - 1, 1) === "/") {
                api_server = api_server.substr(0, api_server.length - 1);
            }
            if (!(api_server.substr(api_server.length - 3, 3) === "api" || api_server.substr(api_server.length - 4, 4) === "api/")) {
                api_server = api_server + "/api";
            }
            saveSettings();
            is_get_status = true;
            is_api_button_press = true;
            getStatus();
        }
    }

    setTimeout(connectEcila, 500)

    $("#api_button, #api_button_ecila, #api_button_ecila_alt").click(connectEcila);

    $("body").click(function () {
        if ($("#options").css('opacity') === 1.0) {
            $('#options').transition({
                opacity: 0.0,
                duration: 100,//animation_rm_duration,
                easing: animation_rm_easing,
                complete: function () {
                    $("#options").css('display', 'none');
                }
            });
        }
    });

    $("#options_button").click(function () {
        if ($("#options").css('display') === 'none') {
            $("#options").css('display', 'block');
            $('#options').transition({
                opacity: 1.0,
                duration: 100,
                easing: animation_rm_easing,
                complete: function () {

                }
            });
        } else {
            $('#options').transition({
                opacity: 0.0,
                duration: 100,
                easing: animation_rm_easing,
                complete: function () {
                    $("#options").css('display', 'none');
                }
            });
        }
    });

    $("#option_select_chat").click(function () {
        if (this_chid !== undefined && !is_send_press) {
            getAllCharaChats();
            let shadowSelectChatPopup = $('#shadow_select_chat_popup').css;
            shadowSelectChatPopup('display', 'block');
            shadowSelectChatPopup.css('opacity', 0.0);
            shadowSelectChatPopup.transition({
                opacity: 1.0,
                duration: animation_rm_duration,
                easing: animation_rm_easing
            });
        }
    });

    $("#option_start_new_chat").click(function () {
        if (this_chid !== undefined && !is_send_press) {
            popup_type = 'new_chat';
            callPopup('<h3>Start new chat?</h3>');
        }
    });

    $("#option_delete_mes").click(function () {
        if (this_chid !== undefined && !is_send_press) {
            $('#dialogue_del_mes').css('display', 'block');
            $('#send_form').css('display', 'none');
            $('.del_checkbox').each(function () {
                if ($(this).parent().attr('mesid') !== 0) {
                    $(this).css("display", "block");
                    $(this).parent().children('.for_checkbox').css('display', 'none');
                }
            });
        }
    });

    $("#dialogue_del_mes_cancel").click(function () {
        $('#dialogue_del_mes').css('display', 'none');
        $('#send_form').css('display', css_send_form_display);
        $('.del_checkbox').each(function () {
            $(this).css("display", "none");
            $(this).parent().children('.for_checkbox').css('display', 'block');
            $(this).parent().css('background', css_mes_bg);
            $(this).prop("checked", false);

        });
        this_del_mes = 0;
    });

    $("#dialogue_del_mes_ok").click(function () {
        $('#dialogue_del_mes').css('display', 'none');
        $('#send_form').css('display', css_send_form_display);
        $('.del_checkbox').each(function () {
            $(this).css("display", "none");
            $(this).parent().children('.for_checkbox').css('display', 'block');
            $(this).parent().css('background', css_mes_bg);
            $(this).prop("checked", false);
        });
        if (this_del_mes !== 0) {
            const selector = $(".mes[mesid='" + this_del_mes + "']")
            selector.nextAll('div').remove();
            selector.remove();
            chat.length = this_del_mes;
            count_view_mes = this_del_mes;
            saveChat();
            const $textchat = $('#chat');
            $textchat.scrollTop($textchat[0].scrollHeight);
        }
        this_del_mes = 0;
    });

    $("#settings_perset").change(function () {
        if ($('#settings_perset').find(":selected").val() !== 'gui') {
            preset_settings = $('#settings_perset').find(":selected").text();
            temp = koboldai_settings[koboldai_setting_names[preset_settings]].temp;
            amount_gen = koboldai_settings[koboldai_setting_names[preset_settings]].genamt;
            rep_pen = koboldai_settings[koboldai_setting_names[preset_settings]].rep_pen;
            rep_pen_size = koboldai_settings[koboldai_setting_names[preset_settings]].rep_pen_range;
            max_context = koboldai_settings[koboldai_setting_names[preset_settings]].max_length;
            $('#temp').val(temp);
            $('#temp_counter').html(temp);

            $('#amount_gen').val(amount_gen);
            $('#amount_gen_counter').html(amount_gen);

            $('#max_context').val(max_context);
            $('#max_context_counter').html(max_context + " Tokens");

            $('#rep_pen').val(rep_pen);
            $('#rep_pen_counter').html(rep_pen);

            $('#rep_pen_size').val(rep_pen_size);
            $('#rep_pen_size_counter').html(rep_pen_size + " Tokens");

            $("#range_block").children().prop("disabled", false);
            $("#range_block").css('opacity', 1.0);
            $("#amount_gen_block").children().prop("disabled", false);
            $("#amount_gen_block").css('opacity', 1.0);

        } else {
            preset_settings = 'gui';
            $("#range_block").children().prop("disabled", true);
            $("#range_block").css('opacity', 0.5);
            $("#amount_gen_block").children().prop("disabled", true);
            $("#amount_gen_block").css('opacity', 0.45);
        }
        saveSettings();
    });
    $("#main_api").change(function () {
        is_get_status = false;
        is_get_status_novel = false;
        is_get_status_ecila = false;
        online_status = 'no_connection';
        checkOnlineStatus();
        changeMainAPI();
        saveSettings();
    });

    function changeMainAPI() {
        if ($('#main_api').find(":selected").val() === 'kobold') {
            $('#kobold_api').css("display", "block");
            $('#ecila_api').css("display", "none");
            $('#novel_api').css("display", "none");
            main_api = 'kobold';
            $('#max_context_block').css('display', 'block');
            $('#amount_gen_block').css('display', 'block');
        }
        if ($('#main_api').find(":selected").val() === 'novel') {
            $('#kobold_api').css("display", "none");
            $('#ecila_api').css("display", "none");
            $('#novel_api').css("display", "block");
            main_api = 'novel';
            $('#max_context_block').css('display', 'none');
            $('#amount_gen_block').css('display', 'none');
        }
        if ($('#main_api').find(":selected").val() === 'ecila') {
            $('#kobold_api').css("display", "none");
            $('#novel_api').css("display", "none");
            $('#ecila_api').css("display", "block");
            main_api = 'ecila';
            $('#max_context_block').css('display', 'none');
            $('#amount_gen_block').css('display', 'none');
        }
    }

    async function getUserAvatars() {
        const response = await fetch("/getuseravatars", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "X-CSRF-Token": token
            },
            body: "{}"

        });
        if (response.ok === true) {
            const getData = await response.json();
            for (let i = 0; i < getData.length; i++) {
                $("#user_avatar_block").append('<div imgfile="' + getData[i] + '" class=avatar><img src="User Avatars/' + getData[i] + '" width=60px height=120px></div>');
            }
        }
    }


    $(document).on('input', '#temp', function () {
        temp = $(this).val();
        if (isInt(temp)) {
            $('#temp_counter').html($(this).val() + ".00");
        } else {
            $('#temp_counter').html($(this).val());
        }
        setTimeout(saveSettings, 500);
    });
    $(document).on('input', '#amount_gen', function () {
        amount_gen = $(this).val();
        $('#amount_gen_counter').html($(this).val());
        setTimeout(saveSettings, 500);
    });
    $(document).on('input', '#max_context', function () {
        max_context = parseInt($(this).val());
        $('#max_context_counter').html($(this).val() + ' Tokens');
        setTimeout(saveSettings, 500);
    });
    $('#style_anchor').change(function () {
        style_anchor = !!$('#style_anchor').prop('checked');
        saveSettings();
    });
    $('#character_anchor').change(function () {
        character_anchor = !!$('#character_anchor').prop('checked');
        saveSettings();
    });
    $(document).on('input', '#rep_pen', function () {
        rep_pen = $(this).val();
        if (isInt(rep_pen)) {
            $('#rep_pen_counter').html($(this).val() + ".00");
        } else {
            $('#rep_pen_counter').html($(this).val());
        }
        setTimeout(saveSettings, 500);
    });
    $(document).on('input', '#rep_pen_size', function () {
        rep_pen_size = $(this).val();
        $('#rep_pen_size_counter').html($(this).val() + " Tokens");
        setTimeout(saveSettings, 500);
    });

    //Novel
    $(document).on('input', '#temp_novel', function () {
        temp_novel = $(this).val();

        if (isInt(temp_novel)) {
            $('#temp_counter_novel').html($(this).val() + ".00");
        } else {
            $('#temp_counter_novel').html($(this).val());
        }
        setTimeout(saveSettings, 500);
    });
    $(document).on('input', '#rep_pen_novel', function () {
        rep_pen_novel = $(this).val();
        if (isInt(rep_pen_novel)) {
            $('#rep_pen_counter_novel').html($(this).val() + ".00");
        } else {
            $('#rep_pen_counter_novel').html($(this).val());
        }
        setTimeout(saveSettings, 500);
    });
    $(document).on('input', '#rep_pen_size_novel', function () {
        rep_pen_size_novel = $(this).val();
        $('#rep_pen_size_counter_novel').html($(this).val() + " Tokens");
        setTimeout(saveSettings, 500);
    });
    //***************SETTINGS****************//
    ///////////////////////////////////////////
    async function getSettings(type) {//timer


        jQuery.ajax({
            type: 'POST',
            url: '/getsettings',
            data: JSON.stringify({}),
            beforeSend: function () {
            },
            cache: false,
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                if (data.result !== 'file not find') {
                    settings = JSON.parse(data.settings);
                    if (settings.username !== undefined) {
                        if (settings.username !== '') {
                            name1 = localStorage.getItem('your_name') || 'User'
                            $('#your_name').val(name1);
                        }
                    }

                    preset_settings = settings.preset_settings;

                    $('#style_anchor').prop('checked', style_anchor);
                    $('#character_anchor').prop('checked', character_anchor);
                    $("#anchor_order option[value=" + anchor_order + "]").attr('selected', 'true');

                    if (preset_settings === 'gui') {
                        $("#settings_perset option[value=gui]").attr('selected', 'true');
                        $("#range_block").children().prop("disabled", true);
                        $("#range_block").css('opacity', 0.5);

                        $("#amount_gen_block").children().prop("disabled", true);
                        $("#amount_gen_block").css('opacity', 0.45);
                    } else {
                        if (typeof koboldai_setting_names[preset_settings] !== 'undefined') {

                            $("#settings_perset option[value=" + koboldai_setting_names[preset_settings] + "]").attr('selected', 'true');
                        } else {
                            $("#range_block").children().prop("disabled", true);
                            $("#range_block").css('opacity', 0.5);
                            $("#amount_gen_block").children().prop("disabled", true);
                            $("#amount_gen_block").css('opacity', 0.45);

                            preset_settings = 'gui';
                            $("#settings_perset option[value=gui]").attr('selected', 'true');
                        }

                    }

                    //User
                    user_avatar = settings.user_avatar;
                    $('.mes').each(function () {
                        if ($(this).attr('ch_name') === name1) {
                            $(this).children('.avatar').children('img').attr('src', 'User Avatars/' + user_avatar);
                        }
                    });

                    api_server = settings.api_server;
                    $('#api_url_text').val(api_server);
                    if (main_api === "ecila") {
                        $('#api_url_text_ecila').val(api_ecila);
                    }
                }

            },
            error: function (jqXHR, exception) {
                console.log(exception);
                console.log(jqXHR);
            }
        });

    }

    async function saveSettings(type) {

        jQuery.ajax({
            type: 'POST',
            url: '/savesettings',
            data: JSON.stringify({
                username: name1,
                api_server: api_server,
                preset_settings: preset_settings,
                preset_settings_novel: preset_settings_novel,
                user_avatar: user_avatar,
                temp: temp,
                amount_gen: amount_gen,
                max_context: max_context,
                anchor_order: anchor_order,
                style_anchor: style_anchor,
                character_anchor: character_anchor,
                main_api: main_api,
                api_key_novel: api_key_novel,
                rep_pen: rep_pen,
                rep_pen_size: rep_pen_size,
                model_novel: model_novel,
                temp_novel: temp_novel,
                rep_pen_novel: rep_pen_novel,
                rep_pen_size_novel: rep_pen_size_novel
            }),
            beforeSend: function () {
            },
            cache: false,
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                if (type === 'change_name') {
                    location.reload();
                }
            },
            error: function (jqXHR, exception) {
                console.log(exception);
                console.log(jqXHR);
            }
        });
    }

    $('#donation').click(function () {
        $('#shadow_tips_popup').css('display', 'block');
        $('#shadow_tips_popup').transition({
            opacity: 1.0,
            duration: 100,
            easing: animation_rm_easing,
            complete: function () {

            }
        });
    });
    $('#tips_cross').click(function () {
        $('#shadow_tips_popup').transition({
            opacity: 0.0,
            duration: 100,
            easing: animation_rm_easing,
            complete: function () {
                $('#shadow_tips_popup').css('display', 'none');
            }
        });
    });
    $('#select_chat_cross').click(function () {
        $('#shadow_select_chat_popup').css('display', 'none');
        $('#load_select_chat_div').css('display', 'block');
    });

    function isInt(value) {
        return !isNaN(value) &&
            parseInt(Number(value)) === value &&
            !isNaN(parseInt(value, 10));
    }

    function hideButtonAndShowConfirmation(element, className) {
        element.css('display', 'none');
        element.parent().children(`.${className}_done`).css('display', 'inline-block');
        element.parent().children(`.${className}_done`).css('opacity', 0.0);
        element.parent().children(`.${className}_cancel`).css('display', 'inline-block');
        element.parent().children(`.${className}_cancel`).css('opacity', 0.0);
        element.parent().children(`.${className}_done`).transition({
            opacity: 1.0,
            duration: 600,
            easing: "",
            complete: function () {
            }
        });
        element.parent().children(`.${className}_cancel`).transition({
            opacity: 1.0,
            duration: 600,
            easing: "",
            complete: function () {
            }
        });
    }

    //********************
    //***Message Deletion***
    $(document).on('click', '.mes_delete', function () {
        if (this_chid !== undefined) {
            let chatScrollPosition = $("#chat").scrollTop();
            if (this_delete_mes_id !== undefined) {
                let mes_deleted = $('#chat').children().filter('[mesid="' + this_delete_mes_id + '"]').children('.mes_block').children('.ch_name').children('.mes_delete_done');
                messageDeleteDone(mes_deleted);
            }

            hideButtonAndShowConfirmation($(this), 'mes_delete')

            const delete_mes_id = $(this).parent().parent().parent().attr('mesid');
            this_delete_mes_id = delete_mes_id;

            if (chat[delete_mes_id] && chat[delete_mes_id]['is_user']) {
                this_delete_mes_chname = name1;
            } else if (chat[delete_mes_id]) {
                this_delete_mes_chname = name2;
            }

            if (this_delete_mes_id === count_view_mes - 1) {
                $("#chat").scrollTop(chatScrollPosition);
            }
        }
    });
    $(document).on('click', '.mes_delete_cancel', function () {
        // $(this).parent().parent().children('.mes_text').empty();
        $(this).css('display', 'none');
        $(this).parent().children('.mes_delete_done').css('display', 'none');
        $(this).parent().children('.mes_delete').css('display', 'inline-block');
        this_delete_mes_id = undefined;
    });
    $(document).on('click', '.mes_delete_done', function () {
        chat.splice(this_delete_mes_id, 1)
        $(this).parent().parent().parent().remove()
        for (let c of document.getElementById("chat").children) {
            const attr = c.attributes.getNamedItem("mesid")
            if (attr.value > this_delete_mes_id) {
                attr.value = "" + (attr.value - 1)
                c.attributes.setNamedItem(attr)
            }
        }
        count_view_mes--
        messageDeleteDone($(this));
    });

    async function messageContinueDone(div) {
        const id = parseInt(this_continue_mes_id) + 1
        const c = chat
            .slice(0, id)
            .map(m => {
                console.log(m)
                return {
                    author: m.name,
                    content: m.mes
                        .replace(/{{user}}/gi, name1)
                        .replace(/{{char}}/gi, name2)
                        .replace(/<USER>/gi, name1)
                        .replace(/<BOT>/gi, name2)
                }
            })

        const author = chat[this_continue_mes_id].name
        const context = $("#description_textarea").val()
            .replace(/{{user}}/gi, name1)
            .replace(/{{char}}/gi, name2)
            .replace(/<USER>/gi, name1)
            .replace(/<BOT>/gi, name2)

        div.parent().children('.loading_message').css('display', 'inline-block');
        div.css('display', 'none');
        div.parent().children('.mes_continue_cancel').css('display', 'none');
        div.parent().children('.mes_continue').css('display', 'inline-block');


        const messages = await generateMessage(author, context, c, true, $('#result_count').val())
        div.parent().children('.loading_message').css('display', 'none');
        if (messages && messages.length === 1) {
            const message = messages?.[0]?.content?.trim?.()

            div.parent().parent().children('.mes_text').children('.edit_textarea').val(message)
            const text = message.trim()
            chat[this_continue_mes_id]['mes'] = text;
            div.parent().parent().children('.mes_text').empty();
            div.parent().parent().children('.mes_text').append(messageFormating(text, this_edit_mes_chname));

            this_continue_mes_id = undefined;

            saveChat();
        } else if (messages && messages.length > 1) {
            const selector = $('#popup_results')
            $('#form_sheld').css('display', 'none')
            selector.html('')
            selector.append('<img id="options_cross" src="img/cross.png" style="float: right; margin: 12px; width: 20px; height: 20px; cursor: pointer; opacity: 0.6" alt="cross">')
            selector.append('<h1 style="color: #FFF;">Most recent messages:</h1>')

            const lastMessages = c.slice(-2)
            for (let lm of lastMessages) {
                displayMessageResults(lm.author, lm.content, null)
            }

            selector.append('<h1 style="color: #FFF;">Select your preferred result below:</h1>')

            for (let i in messages) {
                const message = messages[i]
                displayMessageResults(message.author, message.content, i, 'mes_continue_option')
            }

            $('#chat').css('display', 'none');
            selector.css('display', 'block');
            selector.css('opacity', 0.0);
            selector.transition({
                opacity: 1.0,
                duration: animation_rm_duration,
                easing: animation_rm_easing
            });
        } else {
            console.error('Error')
        }
    }

    async function messageRetryDone(div) {
        const id = parseInt(this_retry_mes_id)
        const c = chat
            .slice(0, id)
            .map(m => {
                console.log(m)
                return {
                    author: m.name,
                    content: m.mes
                        .replace(/{{user}}/gi, name1)
                        .replace(/{{char}}/gi, name2)
                        .replace(/<USER>/gi, name1)
                        .replace(/<BOT>/gi, name2)
                }
            })

        const author = chat[this_retry_mes_id].name
        const context = $("#description_textarea").val()
            .replace(/{{user}}/gi, name1)
            .replace(/{{char}}/gi, name2)
            .replace(/<USER>/gi, name1)
            .replace(/<BOT>/gi, name2)

        div.parent().children('.loading_message').css('display', 'inline-block');
        div.css('display', 'none');
        div.parent().children('.mes_retry_cancel').css('display', 'none');
        div.parent().children('.mes_retry').css('display', 'inline-block');

        const messages = await generateMessage(author, context, c, false, $('#result_count').val())
        div.parent().children('.loading_message').css('display', 'none');
        if (messages && messages.length === 1) {
            const message = messages?.[0]?.content?.trim?.()
            const text = message.trim()

            div.parent().parent().children('.mes_text').children('.edit_textarea').val(text)
            chat[this_retry_mes_id]['mes'] = text;
            div.parent().parent().children('.mes_text').empty();
            div.parent().parent().children('.mes_text').append(messageFormating(text, this_edit_mes_chname));

            this_retry_mes_id = undefined;

            saveChat();
        } else if (messages && messages.length > 1) {
            const selector = $('#popup_results')
            $('#form_sheld').css('display', 'none')
            selector.html('')
            selector.append('<img id="options_cross" src="img/cross.png" style="float: right; margin: 12px; width: 20px; height: 20px; cursor: pointer; opacity: 0.6" alt="cross">')
            selector.append('<h1 style="color: #FFF;">Most recent messages:</h1>')

            const lastMessages = c.slice(-2)
            for (let lm of lastMessages) {
                displayMessageResults(lm.author, lm.content, null)
            }

            selector.append('<h1 style="color: #FFF;">Select your preferred result below:</h1>')

            for (let i in messages) {
                const message = messages[i]
                displayMessageResults(message.author, message.content, i)
            }

            $('#chat').css('display', 'none');
            selector.css('display', 'block');
            selector.css('opacity', 0.0);
            selector.transition({
                opacity: 1.0,
                duration: animation_rm_duration,
                easing: animation_rm_easing
            });
        } else {
            console.error('Error')
        }
    }

    $("#result_count").change(function () {
        localStorage.setItem('result_count', $(this).val())
    });

    $("#settings_enable_alternative_tts").change(function () {
        localStorage.setItem('settings_enable_alternative_tts', $(this).val())
    });

    $("#settings_11lab_api_key").change(function () {
        localStorage.setItem('settings_11lab_api_key', $(this).val())
    });

    $("#settings_11lab_voice_id").change(function () {
        localStorage.setItem('settings_11lab_voice_id', $(this).val())
    });

    $("#settings_11lab_your_voice_id").change(function () {
        localStorage.setItem('settings_11lab_your_voice_id', $(this).val())
    });

    $("#settings_11lab_stability").change(function () {
        localStorage.setItem('settings_11lab_stability', $(this).val())
    });

    $("#settings_11lab_similarity").change(function () {
        localStorage.setItem('settings_11lab_similarity', $(this).val())
    });

    $("#settings_11lab_your_stability").change(function () {
        localStorage.setItem('settings_11lab_your_stability', $(this).val())
    });

    $("#settings_11lab_your_similarity").change(function () {
        localStorage.setItem('settings_11lab_your_similarity', $(this).val())
    });

    $("#settings_enable_autoplay_tts").change(function () {
        localStorage.setItem('settings_enable_autoplay_tts', $(this).val())
    });

    $("#settings_enable_tts_streamer_mode").change(function () {
        localStorage.setItem('settings_enable_tts_streamer_mode', $(this).val())
    });

    $(document).on('click', '#options_cross', function () {
        const selector = $('#popup_results')
        selector.css('display', 'none');
        selector.css('opacity', 0.0);
        $('#chat').css('display', 'block');
        $('#form_sheld').css('display', 'block')
        this_retry_mes_id = undefined
        this_continue_mes_id = undefined
    });

    $(document).on('click', '.mes_retry_option', function () {
        $('#form_sheld').css('display', 'block')
        $('#popup_results').css('display', 'none');
        $('#chat').css('display', 'block')

        const optionMessage = $('#popup_results').children().filter('[mesid="' + $(this).attr('mesid') + '"]').children('.mes_block').children('.mes_text').html()
        const optionMessageRaw = $('#popup_results').children().filter('[mesid="' + $(this).attr('mesid') + '"]').children('.mes_block').children('.mes_text_raw').text()
        $('#chat').children().filter('[mesid="' + this_retry_mes_id + '"]').children('.mes_block').children('.mes_text').html(optionMessage);
        chat[this_retry_mes_id]['mes'] = optionMessageRaw
        saveChat()
        this_retry_mes_id = undefined
    })

    $(document).on('click', '.mes_continue_option', function () {
        $('#form_sheld').css('display', 'block')
        $('#popup_results').css('display', 'none');
        $('#chat').css('display', 'block')

        const optionMessage = $('#popup_results').children().filter('[mesid="' + $(this).attr('mesid') + '"]').children('.mes_block').children('.mes_text').html()
        const optionMessageRaw = $('#popup_results').children().filter('[mesid="' + $(this).attr('mesid') + '"]').children('.mes_block').children('.mes_text_raw').text()
        $('#chat').children().filter('[mesid="' + this_continue_mes_id + '"]').children('.mes_block').children('.mes_text').html(optionMessage);
        chat[this_continue_mes_id]['mes'] = optionMessageRaw
        saveChat()
        this_continue_mes_id = undefined
    })

    function displayMessageResults(characterName, messageText, i = null, className = "mes_retry_option") {
        let avatarImg = "User Avatars/" + user_avatar;
        const messageHtml = messageFormating(messageText, characterName);

        if (characterName === name2) {
            if (this_chid === null) {
                avatarImg = "img/ecila.png";
            } else {
                if (characters[this_chid].avatar !== 'none') {
                    avatarImg = characters[this_chid].avatar?.startsWith?.('http') ?
                        characters[this_chid].avatar
                        : ("characters/" + characters[this_chid].avatar);
                    if (is_mes_reload_avatar !== false) {
                        avatarImg += "#" + is_mes_reload_avatar;
                    }
                } else {
                    avatarImg = "img/fluffy.png";
                }
            }
        }

        const divAvatar = `<div class=avatar><img src='${avatarImg}'></div>`

        const newMessage = i !== null

        if (newMessage) {
            $('#popup_results').append(
                `<div class='mes ${className}' mesid=`
                + i
                + " ch_name="
                + characterName
                + ">"
                + "<div class='character_name' hidden>" + characterName + "</div>"
                + "<div class='for_checkbox'></div>"
                + "<input type='checkbox' class='del_checkbox'>"
                + divAvatar
                + "<div class=mes_block>"
                + "<div class=ch_name>"
                + characterName

                + "</div>"
                + "<div class=mes_text>"
                + messageHtml
                + "</div>"
                + "<div class=mes_text_raw hidden>"
                + messageText
                + "</div>"

                + "</div>"
                + "</div>"
            );
        } else {
            $('#popup_results').append(
                "<div class='mes' ch_name="
                + characterName
                + ">"
                + "<div class='character_name' hidden>" + characterName + "</div>"
                + "<div class='for_checkbox'></div>"
                + "<input type='checkbox' class='del_checkbox'>"
                + divAvatar
                + "<div class=mes_block>"
                + "<div class=ch_name>"
                + characterName

                + "</div>"
                + "<div class=mes_text>"
                + messageHtml
                + "</div>"
                + "<div class=mes_text_raw hidden>"
                + messageText
                + "</div>"

                + "</div>"
                + "</div>"
            );
        }
    }

    //********************
    //***Message Continuation***
    $(document).on('click', '.mes_continue', function () {
        if (this_chid !== undefined) {
            if (this_continue_mes_id !== undefined) {
                let mes_continued = $('#chat').children().filter('[mesid="' + this_continue_mes_id + '"]').children('.mes_block').children('.ch_name').children('.mes_delete_done');
                messageContinueDone(mes_continued);
            }

            hideButtonAndShowConfirmation($(this), 'mes_continue')

            this_continue_mes_id = $(this).parent().parent().parent().attr('mesid');
        }
    });
    $(document).on('click', '.mes_continue_cancel', function () {
        $(this).css('display', 'none');
        $(this).parent().children('.mes_continue_done').css('display', 'none');
        $(this).parent().children('.mes_continue').css('display', 'inline-block');
        this_continue_mes_id = undefined;
    });
    $(document).on('click', '.mes_continue_done', function () {
        messageContinueDone($(this));
    });

    async function copyToClipboard(textToCopy) {
        // Navigator clipboard api needs a secure context (https)
        if (navigator.clipboard && window.isSecureContext) {
            await navigator.clipboard.writeText(textToCopy);
        } else {
            // Use the 'out of viewport hidden text area' trick
            const textArea = document.createElement("textarea");
            textArea.value = textToCopy;

            // Move textarea out of the viewport so it's not visible
            textArea.style.position = "absolute";
            textArea.style.left = "-999999px";

            document.body.prepend(textArea);
            textArea.select();

            try {
                document.execCommand('copy');
            } catch (error) {
                console.error(error);
            } finally {
                textArea.remove();
            }
        }
    }

    $(document).on('click', '.copy_message', async function () {
        const id = $(this).parent().parent().parent().attr('mesid');
        console.log(chat[id].mes)
        await copyToClipboard(chat[id].mes)
    });

    $(document).on('click', '.speak_message', async function () {
        const id = $(this).parent().parent().parent().attr('mesid');
        const selector = $(this).parent().children('audio')
        selector.css('display', 'inline-block')
        const enableThirdPersonVoice = $('#settings_enable_third_person_voice').val() === 'true'

        const useElevenLabs = $('#settings_enable_alternative_tts').val() === 'true'
        const streamerMode = $('#settings_enable_tts_streamer_mode').val() === 'true'

        const seed = streamerMode ? $('#character_voice').val() : chat[id].is_user ? your_voice : $('#character_voice').val()
        const third_person_seed = useElevenLabs ?
            $('#character_voice_third_person').val()
            : $('#character_voice_third_person').val()

        const contentAuthor = chat[id].mes.match(/^\(as ([^)]*)\)/i)?.[1]?.trim?.()
        const content = `${chat[id].mes.replace(/^\(as [^)]*\)/i, '').trim()}` || ''

        const finalAuthor = contentAuthor ? contentAuthor : $('#your_name').val()

        const text = (streamerMode && chat[id].is_user ? `${finalAuthor} ${chat[id].mes.endsWith('?') ? 'asks' : 'says'}. ` : '')
            + (enableThirdPersonVoice ?
                content
                    .replace(/  +/g, ' ')
                    .replace(/~/g, '')
                : content
                    .replace(/\*[^*]*\*/gi, '') // removes parts in between asterisks
                    .replace(/  +/g, ' ')       // removes double spaces
                    .replace(/~/g, ''))          // replaces tilde

        if (useElevenLabs) {
            if (enableThirdPersonVoice) {
                selector.attr('src', `/tts2?access_token=${$("#settings_11lab_api_key").val()}&text=${text}&voice_id=${$("#settings_11lab_voice_id").val()}&stability=${$("#settings_11lab_stability").val()}&similarity=${$("#settings_11lab_similarity").val()}&third_person_voice_id=${$("#settings_11lab_your_voice_id").val()}&third_person_stability=${$("#settings_11lab_your_stability").val()}&third_person_similarity=${$("#settings_11lab_your_similarity").val()}`)
            } else {
                selector.attr('src', `/tts?access_token=${$("#settings_11lab_api_key").val()}&text=${text}&voice_id=${$("#settings_11lab_voice_id").val()}&stability=${$("#settings_11lab_stability").val()}&similarity=${$("#settings_11lab_similarity").val()}`)
            }
        } else {
            if (enableThirdPersonVoice) {
                selector.attr('src', `http://86.214.101.142:3000/api/v1/generate/textToSpeech2?access_token=${api_key_ecila}&seed=${seed}&text=${text}&third_person_seed=${third_person_seed}`)
            } else {
                selector.attr('src', `http://86.214.101.142:3000/api/v1/generate/textToSpeech?access_token=${api_key_ecila}&seed=${seed}&text=${text}`)
            }
        }


        selector[0].play()
    });

    //********************
    //***Message Retry***
    $(document).on('click', '.mes_retry', function () {
        if (this_chid !== undefined) {
            if (this_retry_mes_id !== undefined) {
                let mes_continued = $('#chat').children().filter('[mesid="' + this_retry_mes_id + '"]').children('.mes_block').children('.ch_name').children('.mes_delete_done');
                messageRetryDone(mes_continued);
            }

            hideButtonAndShowConfirmation($(this), 'mes_retry')

            this_retry_mes_id = $(this).parent().parent().parent().attr('mesid');
        }
    });
    $(document).on('click', '.mes_retry_cancel', function () {
        $(this).css('display', 'none');
        $(this).parent().children('.mes_retry_done').css('display', 'none');
        $(this).parent().children('.mes_retry').css('display', 'inline-block');
        this_retry_mes_id = undefined;
    });
    $(document).on('click', '.mes_retry_done', function () {
        messageRetryDone($(this));
    });

    //********************
    //***Message Editor***
    $(document).on('click', '.mes_edit', function () {
        if (this_chid !== undefined) {
            let chatScrollPosition = $("#chat").scrollTop();
            if (this_edit_mes_id !== undefined) {
                let mes_edited = $('#chat').children().filter('[mesid="' + this_edit_mes_id + '"]').children('.mes_block').children('.ch_name').children('.mes_edit_done');
                messageEditDone(mes_edited);
            }
            $(this).parent().parent().children('.mes_text').empty();

            hideButtonAndShowConfirmation($(this), 'mes_edit')

            const edit_mes_id = $(this).parent().parent().parent().attr('mesid');
            this_edit_mes_id = edit_mes_id;

            let text = chat[edit_mes_id]['mes'];
            if (chat[edit_mes_id]['is_user']) {
                this_edit_mes_chname = name1;
            } else {
                this_edit_mes_chname = name2;
            }
            text = text.trim();
            $(this).parent().parent().children('.mes_text').append('<textarea class=edit_textarea>' + text + '</textarea>');
            let edit_textarea = $(this).parent().parent().children('.mes_text').children('.edit_textarea');
            edit_textarea.css('opacity', 0.0);
            edit_textarea.transition({
                opacity: 1.0,
                duration: 0,
                easing: "",
                complete: function () {
                }
            });
            edit_textarea.height(0);
            edit_textarea.height(edit_textarea[0].scrollHeight);
            edit_textarea.focus();
            edit_textarea[0].setSelectionRange(edit_textarea.val().length, edit_textarea.val().length);
            if (this_edit_mes_id === count_view_mes - 1) {
                $("#chat").scrollTop(chatScrollPosition);
            }
        }
    });
    $(document).on('click', '.mes_edit_cancel', function () {
        const text = chat[this_edit_mes_id]['mes'];

        $(this).parent().parent().children('.mes_text').empty();
        $(this).css('display', 'none');
        $(this).parent().children('.mes_edit_done').css('display', 'none');
        $(this).parent().children('.mes_edit').css('display', 'inline-block');
        $(this).parent().parent().children('.mes_text').append(messageFormating(text, this_edit_mes_chname));
        this_edit_mes_id = undefined;
    });
    $(document).on('click', '.mes_edit_done', function () {
        messageEditDone($(this));
    });

    function messageEditDone(div) {

        let text = div.parent().parent().children('.mes_text').children('.edit_textarea').val();
        text = text?.trim() ?? '';
        chat[this_edit_mes_id]['mes'] = text;
        div.parent().parent().children('.mes_text').empty();
        div.css('display', 'none');
        div.parent().children('.mes_edit_cancel').css('display', 'none');
        div.parent().children('.mes_edit').css('display', 'inline-block');
        div.parent().parent().children('.mes_text').append(messageFormating(text, this_edit_mes_chname));
        this_edit_mes_id = undefined;
        saveChat();
    }

    function messageDeleteDone(div) {
        div.css('display', 'none');
        div.parent().children('.mes_delete_cancel').css('display', 'none');
        div.parent().children('.mes_delete').css('display', 'inline-block');
        this_delete_mes_id = undefined;
        saveChat();
    }

    $("#your_name_button").click(function () {
        if (!is_send_press) {
            name1 = $("#your_name").val();
            if (name1 === undefined || name1 === '') name1 = default_user_name;
            console.log(name1);

            localStorage.setItem('your_name', name1)
        }
    });

    //Select chat
    async function getAllCharaChats() {
        $('#select_chat_div').html('');
        jQuery.ajax({
            type: 'POST',
            url: '/getallchatsofchatacter',
            data: JSON.stringify({avatar_url: characters[this_chid].avatar}),
            beforeSend: function () {
            },
            cache: false,
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                $('#load_select_chat_div').css('display', 'none');
                let dataArr = Object.values(data);
                data = dataArr.sort((a, b) => a['file_name'].localeCompare(b['file_name']));
                data = data.reverse();
                for (const key in data) {
                    let strlen = 40;
                    let mes = data[key]['mes'];
                    if (mes && mes.length > strlen) {
                        mes = '...' + mes.substring(mes.length - strlen);
                    }
                    $('#select_chat_div').append('<div class="select_chat_block" file_name="' + data[key]['file_name'] + '"><div class=avatar><img src="characters/' + characters[this_chid]['avatar'] + '" style="width: 33px; height: 51px;"></div><div class="select_chat_block_filename">' + data[key]['file_name'] + '</div><div class="select_chat_block_mes">' + mes + '</div></div>');
                    if (characters[this_chid]['chat'] === data[key]['file_name'].replace('.jsonl', '')) {
                        //children().last()
                        $('#select_chat_div').children(':nth-last-child(1)').attr('highlight', true);
                    }
                }
            },
            error: function (jqXHR, exception) {
                console.log(exception);
                console.log(jqXHR);
            }
        });
    }

    function saveApiKeyEcila() {
        const value = $('#api_key_ecila').val()
        api_key_ecila = value?.trim()
        localStorage.setItem("api_key_ecila", api_key_ecila)
        saveSettings();
        is_get_status_ecila = true;
        is_api_button_press_ecila = true;
    }

    $("#api_key_ecila").change(saveApiKeyEcila)
    $(document).on('click', '#api_button_ecila', saveApiKeyEcila);

    $(document).on('click', '#api_button_ecila_alt', async function () {
        const altValue = $('#api_key_ecila_alt').val();
        if (altValue !== '') {
            api_key_ecila = altValue?.trim()
            localStorage.setItem("api_key_ecila", api_key_ecila)
            $('#api_key_ecila').val(api_key_ecila)
            saveSettings();
            is_get_status_ecila = true;
            is_api_button_press_ecila = true;

            addOneMessage(interactiveTutorial[2].message)
            chat.push(interactiveTutorial[2].message)
            await new Promise(r => setTimeout(r, 3000))
            addOneMessage(interactiveTutorial[3].message)
            chat.push(interactiveTutorial[3].message)
        }
    });

    //**************************CHARACTER IMPORT EXPORT*************************//
    $("#character_import_button").click(function () {
        $("#character_import_file").click();
    });

    $("#character_import_file").on("change", function (e) {
        $('#rm_info_avatar').html('');
        const file = e.target.files[0];
        if (!file) {
            return;
        }
        const ext = file.name.match(/\.(\w+)$/);
        if (!ext || (ext[1].toLowerCase() !== "json" && ext[1].toLowerCase() !== "png")) {
            return;
        }

        const format = ext[1].toLowerCase();
        $("#character_import_file_type").val(format);
        const formData = new FormData($("#form_import").get(0));
        const selector = $('#rm_info_block')

        jQuery.ajax({
            type: 'POST',
            url: '/importcharacter',
            data: formData,
            beforeSend: function () {
            },
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.file_name !== undefined) {

                    selector.transition({opacity: 0, duration: 0});
                    const $prev_img = $('#avatar_div_div').clone();
                    $prev_img.children('img').attr('src', 'characters/' + data.file_name + '.png');
                    $('#rm_info_avatar').append($prev_img);
                    select_rm_info("Character created");

                    selector.transition({opacity: 1.0, duration: 2000});
                    getCharacters();
                }
            },
            error: function (jqXHR, exception) {
                $('#create_button').removeAttr("disabled");
            }
        });
    });

    $('#export_button').click(function () {
        console.log(characters[this_chid])

        /*
        const link = document.createElement('a');
        link.href = 'characters/' + characters[this_chid].avatar;
        link.download = characters[this_chid].avatar;
        document.body.appendChild(link);
        link.click();
        */
    });

    $('#reset_chat_button').click(function () {
        clearChat();
        chat.length = 0;
        saveChat()
        getChat();
    });


    //**************************CHAT IMPORT EXPORT*************************//
    $("#chat_import_button").click(function () {
        $("#chat_import_file").click();
    });

    $("#chat_import_file").on("change", function (e) {
        const file = e.target.files[0];
        if (!file) {
            return;
        }
        const ext = file.name.match(/\.(\w+)$/);
        if (!ext || (ext[1].toLowerCase() !== "json" && ext[1].toLowerCase() !== "jsonl")) {
            return;
        }

        const format = ext[1].toLowerCase();
        $("#chat_import_file_type").val(format);
        const formData = new FormData($("#form_import_chat").get(0));

        jQuery.ajax({
            type: 'POST',
            url: '/importchat',
            data: formData,
            beforeSend: function () {
                $('#select_chat_div').html('');
                $('#load_select_chat_div').css('display', 'block');
            },
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.res) {
                    getAllCharaChats();
                }
            },
            error: function (jqXHR, exception) {
                $('#create_button').removeAttr("disabled");
            }
        });
    });

    $(document).on('click', '.select_chat_block', function () {
        let file_name = $(this).attr("file_name").replace('.jsonl', '');
        characters[this_chid]['chat'] = file_name;
        clearChat();
        chat.length = 0;
        getChat();
        $('#selected_chat_pole').val(file_name);
        $("#create_button").click();
        $('#shadow_select_chat_popup').css('display', 'none');
        $('#load_select_chat_div').css('display', 'block');
    });

    class Tutorial {
        static index = 0

        static async run() {
            const tutorialEntry = interactiveTutorial[Tutorial.index]

            if (!tutorial_finished && this_chid === null) {
                for (let i = 0; i < interactiveTutorial.length; i++) {
                    const message = tutorialEntry.message
                    await new Promise(r => setTimeout(r, 3000))
                    if (tutorial_finished || this_chid !== null) break
                    addOneMessage(message)
                    chat.push(message)
                    if (!message.continue) break
                }
            }
        }
    }

    // Tutorial.run().then()
    async function generateFrontPageImage() {
        await new Promise(r => setTimeout(r, 500))
        $('#img_front_page').attr("src", 'img/load2.gif')

        const nsfw = window.location.href.includes('?nsfw=true')
        jQuery.ajax({
            type: 'POST',
            url: '/textToImageNovelAI',
            data: JSON.stringify({
                access_token: api_key_ecila,
                "X-CSRF-Token": token,
                "prompt": `closeup portrait, Ecila, female AI, happy, dancing, laughing, tavern in the background, thin frame with small breasts, tall, [[[tentacles]]] redhead red tentacle hair, green eyes, light brown skin tone, ${!nsfw ? 'black dress' : 'nude, naked'}, cute smile, cute face, {{{realism}}}, {{dramatic lighting}}, by greg rutkowski`,
                "width": nsfw ? 512 : 768,
                "height": nsfw ? 768 : 512,
                "negativePrompt": nsfw ? "big breasts" : undefined
            }),
            beforeSend: function () {

            },
            cache: false,
            dataType: "json",
            crossDomain: true,
            contentType: "application/json",
            success: function (result) {
                console.log("result", result)
                // $('#img_front_page').attr("src", 'data:image/png;base64,' + result.data)
            },
            error: function (jqXHR, exception) {
                // console.log(exception);
                // console.log(jqXHR);
                if (jqXHR.status === 200) {
                    $('#img_front_page').attr("src", 'data:image/png;base64,' + jqXHR.responseText)
                    $('#img_front_page').css('opacity', '0')
                    $('#img_front_page').transition({
                        opacity: 1.0,
                        duration: 700,
                        easing: "",
                        complete: function () {
                        }
                    });
                } else {
                    $('#img_front_page').attr("src", 'img/star_dust_city.png')
                    online_status = 'no_connection';
                    resultCheckStatus();
                }
            }
        })
    }

    generateFrontPageImage().then()
});
